package egovframework.oacis.web.admin;

import egovframework.oacis.service.MemberService;
import egovframework.oacis.service.MemberVO;
import egovframework.oacis.web.MemberController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Controller("/")
@RequestMapping("/admin")
public class OacisAdminController {

	private static final Logger LOGGER = LoggerFactory.getLogger(OacisAdminController.class);

	@Autowired
	private MemberService memberService;

	/**
	 * [관리자] 메인
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String index(HttpServletRequest request) throws Exception {
		MemberVO vo = (MemberVO) request.getSession().getAttribute("memberVo");
		if (vo == null || !vo.getAdmin().equals("9")) {
			return "redirect:/admin/login.do";
		}
		return "redirect:/admin/business.do";
	}

	@RequestMapping("/login")
	public String adminLogin() {
		return "/admin/login";
	}

	@RequestMapping(value = "/actionLogin")
	public String adminActionLogin(
			@ModelAttribute("memberVO") MemberVO memberVO
			, HttpServletRequest request
			, RedirectAttributes rttr) throws Exception {

		MemberVO vo  = memberService.loginAction(memberVO);
		if (vo != null && !("").equals(vo.getEmail()) && vo.getAdmin().equals("9")) {
			request.getSession().setAttribute("memberVO", vo);
			rttr.addFlashAttribute("message","로그인 되었습니다.");
			return "redirect:/admin/business.do";
		} else {
			rttr.addFlashAttribute("message","이메일 또는 패스워드를 확인하세요.");
			return "redirect:/admin/login.do";
		}
	}

	/**
	 * 로그아웃
	 * */
	@RequestMapping(value = "/logout")
	public String adminLogout(
			HttpServletRequest request
			, RedirectAttributes rttr) {

		request.getSession().setAttribute("memberVO", null);
		rttr.addFlashAttribute("message","로그아웃 되었습니다.");
		return "redirect:/admin/login.do";
	}

}
