package egovframework.oacis.web;

import egovframework.oacis.service.BusinessVO;
import egovframework.oacis.service.FinanceService;
import egovframework.oacis.service.FinanceVO;
import egovframework.oacis.service.MemberVO;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@Controller
public class FinanceController {

    @Autowired
    protected FinanceService financeService;

    @RequestMapping(value = "/financing.do")
    public String viewFinance(@ModelAttribute("financeVO") FinanceVO financeVO, ModelMap model){
        List<HashMap> financeList = financeService.selectFinanceList(financeVO);
        int totalCount = financeService.selectFinanceCount();
//        LOGGER.debug(String.valueOf(totalCount));
        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(financeVO.page);
        paginationInfo.setPageSize(financeVO.pageSize);
        paginationInfo.setTotalRecordCount(totalCount);
        paginationInfo.setRecordCountPerPage(financeVO.recordCountPerPage);
        model.addAttribute("paginationInfo", paginationInfo);
        model.addAttribute("financeList", financeList);
        return "/financing";
    }

    @RequestMapping(value = "/registFinance.do")
    public String registFinance(@ModelAttribute("financeVO") FinanceVO financeVO, ModelMap model, HttpServletRequest request){
        if("Y".equals(financeVO.getUpdateYn())){
            int rtn  = financeService.updateFinance(financeVO);
            if(rtn == 1){
                model.addAttribute("message","success");
            }else{
                model.addAttribute("message","fail");
            }
        }else{
            MemberVO vo = (MemberVO) request.getSession().getAttribute("memberVO");
            if( vo != null){
                financeVO.setCreater(vo.getEmail());
            }
            int rtn = financeService.registFinance(financeVO);
            if(rtn == 1){
                model.addAttribute("message","success");
            }else{
                model.addAttribute("message","fail");
            }

        }
        return "redirect:/financing.do";
    }

    @RequestMapping(value = "/viewRegistFinance.do")
    public String viewRegistFinance(@ModelAttribute("financeVO") FinanceVO financeVO, ModelMap model){
        if("Y".equals(financeVO.getUpdateYn())){
            HashMap financeData =  financeService.selectFinance(financeVO);
            financeData.put("updateYn","Y");
            model.addAttribute("financeData", financeData);
        }
        return "/registFinancing";
    }

    @RequestMapping(value = "/viewUpdateFinance.do")
    public String viewUpdateFinance(@ModelAttribute("financeVO") FinanceVO financeVO, ModelMap model){
        HashMap map = financeService.selectFinance(financeVO);
        model.addAttribute("finance", map);
        return "/modifyFinancing";
    }

    @RequestMapping(value = "/deleteFinance.do")
    public String deleteFinance(@ModelAttribute("financeVO") FinanceVO financeVO){
        financeService.deleteFinance(financeVO);
        return "/financing";
    }

    /// ADMIN ======================
    /**
     * [관리자] 금융정보 관리
     * */
    @RequestMapping("/admin/financing")
    public String adminFinancing(@ModelAttribute("financeVO") FinanceVO financeVO, ModelMap model) throws Exception {
        List<HashMap> financeList = financeService.selectFinanceList(financeVO);
        int totalCount = financeService.selectFinanceCount();
//        LOGGER.debug(String.valueOf(totalCount));
        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(financeVO.page);
        paginationInfo.setPageSize(financeVO.pageSize);
        paginationInfo.setTotalRecordCount(totalCount);
        paginationInfo.setRecordCountPerPage(financeVO.recordCountPerPage);
        model.addAttribute("paginationInfo", paginationInfo);
        model.addAttribute("financeList", financeList);
        return "/admin/financing";
    }

    /**
     * [관리자] 금융정보 수정정보
     * */
    @RequestMapping("/admin/modifyFinancing")
    public String adminFinancingModify(HttpServletRequest request, ModelMap model,FinanceVO financeVO) throws Exception {
        HashMap financeData =  financeService.selectFinance(financeVO);
        financeData.put("updateYn","Y");
        model.addAttribute("financeData", financeData);
        return "/admin/modifyFinancing";
    }

    /**
     * [관리자] 금융정보 수정 / 입력
     * */
    @RequestMapping("/admin/registFinancing")
    public String adminFinancingRegist(HttpServletRequest request, ModelMap model,FinanceVO financeVO) throws Exception {
        if("Y".equals(financeVO.getUpdateYn())){
            int rtn  = financeService.updateFinance(financeVO);
            if(rtn == 1){
                model.addAttribute("message","success");
            }else{
                model.addAttribute("message","fail");
            }
        }else{
            MemberVO vo = (MemberVO) request.getSession().getAttribute("memberVO");
            if( vo != null){
                financeVO.setCreater(vo.getEmail());
            }
            int rtn = financeService.registFinance(financeVO);
            if(rtn == 1){
                model.addAttribute("message","success");
            }else{
                model.addAttribute("message","fail");
            }

        }
        return "redirect:/admin/financing";
    }

    /**
     * [관리자] 금융정보 삭제
     * */
    @RequestMapping(value = "/admin/deleteFinance.do")
    public String adminDeleteFinance(@ModelAttribute("financeVO") FinanceVO financeVO){
        financeService.deleteFinance(financeVO);
        return "redirect:/admin/financing";
    }

    @RequestMapping(value = "/admin/viewRegistFinance.do")
    public String viewAdminRegistFinance(@ModelAttribute("financeVO") FinanceVO financeVO, ModelMap model){
        if("Y".equals(financeVO.getUpdateYn())){
            HashMap financeData =  financeService.selectFinance(financeVO);
            financeData.put("updateYn","Y");
            model.addAttribute("financeData", financeData);
        }
        return "/admin/addFinancing";
    }
}
