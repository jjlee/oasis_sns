package egovframework.oacis.web;

import egovframework.oacis.service.ArticlesVO;
import egovframework.oacis.service.SurveyService;
import egovframework.oacis.service.SurveyVO;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@Controller
public class SurveyController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SurveyController.class);

    @Autowired
    private SurveyService serveyService;

    /**
     * 설문조사
     */
    @RequestMapping(value = "/survey.do")
    public String viewSurvey(ModelMap model
            , @ModelAttribute("surveyVO") SurveyVO surveyVO
            , HttpServletRequest request) throws Exception {

        int totalCount = serveyService.selectSurveyCount();

        List<HashMap> list = serveyService.selectSurveyList(surveyVO);
        model.addAttribute("list", list);

        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(surveyVO.page);
        paginationInfo.setPageSize(surveyVO.pageSize);
        paginationInfo.setTotalRecordCount(totalCount);
        paginationInfo.setRecordCountPerPage(surveyVO.recordCountPerPage);
        model.addAttribute("paginationInfo", paginationInfo);
        return "/survey";
    }

    @RequestMapping(value = "/surveyDetail.do")
    public String viewSurveyDetail(ModelMap model
            , @ModelAttribute("surveyVO") SurveyVO surveyVO
            , HttpServletRequest request) throws Exception {

        HashMap map = serveyService.selectSurveyDetail(surveyVO);
        model.addAttribute("info", map);

        List<HashMap> qList = serveyService.selectSurveyDetailQuestions(surveyVO);

        for (int i = 0; i < qList.stream().count(); i++ ) {
            HashMap q = qList.get(i);
            int no = (int) q.get("no");
            List aList = serveyService.selectSurveyDetailAnswers(no);
            q.put("answers", aList);
        }

        model.addAttribute("qList", qList);
        return "/surveyDetail";
    }


    /**
     * [ADMIN] =========================================================
     */
    @RequestMapping("/admin/survey")
    public String adminSurvey(ModelMap model
            , @ModelAttribute("surveyVO") SurveyVO surveyVO) throws Exception {

        int totalCount = serveyService.selectSurveyAllCount();

        List<HashMap> list = serveyService.selectSurveyAllList(surveyVO);
        model.addAttribute("list", list);

        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(surveyVO.page);
        paginationInfo.setPageSize(surveyVO.pageSize);
        paginationInfo.setTotalRecordCount(totalCount);
        paginationInfo.setRecordCountPerPage(surveyVO.recordCountPerPage);
        model.addAttribute("paginationInfo", paginationInfo);
        return "admin/survey/survey";
    }

    @ResponseBody
    @RequestMapping("/admin/deleteSurvey")
    public String adminDeleteSurvey(SurveyVO vo) throws Exception {
        int result = serveyService.deleteSurvey(vo);
        return String.valueOf(result);
    }

    @ResponseBody
    @RequestMapping("/admin/showSurvey")
    public String adminShowSurvey(SurveyVO vo) throws Exception {
        int result = serveyService.showSurvey(vo);
        return String.valueOf(result);
    }

}
