package egovframework.oacis.web;

import egovframework.oacis.service.TextsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class TextsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TextsController.class);

    @Autowired
    private TextsService textsService;

    /**
     * OACIS 소개
     */
    @RequestMapping(value = "/oasis.do")
    public String viewOacis(ModelMap model) throws Exception {
        HashMap contents = textsService.selectTextsContents("INTRODUCTION");
        model.addAttribute("contents", contents);
        return "/oacis";
    }

    /**
     * OACIS 소개
     */
    @RequestMapping(value = "/useGuide.do")
    public String viewUseGuide(ModelMap model) throws Exception {
        HashMap contents = textsService.selectTextsContents("USE_GUIDE");
        model.addAttribute("contents", contents);
        return "/oacis";
    }

    /**
     * [ADMIN] =========================================================
     */
    @RequestMapping("/admin/contents")
    public String adminContents(ModelMap model) throws Exception {
        List<HashMap> list = textsService.selectTextsList();
        model.addAttribute("list", list);
        return "/admin/contents/contents";
    }

    @RequestMapping("/admin/modifyContents")
    public String adminContentsModify(
            ModelMap model
            , @RequestParam Map<String, String> param) throws Exception {
        HashMap map = textsService.selectTextsContents(param.get("title"));
        model.addAttribute("c", map);
        return "/admin/contents/modifyContents";
    }

    @RequestMapping("/admin/actionTexts")
    public String adminContentsAction(
            ModelMap model
            , @RequestParam Map<String, String> param) throws Exception {
        HashMap map = new HashMap();
        map.put("no", param.get("text_no"));
        map.put("title", param.get("text_title"));
        map.put("comment", param.get("text_comment"));
        map.put("body", param.get("text_body"));
        textsService.updateTextContents(map);

        HashMap rMap = textsService.selectTextsContents(param.get("text_title"));
        model.addAttribute("c", rMap);
        model.addAttribute("message", "저장되었습니다.");
        return "/admin/contents/modifyContents";
    }
}
