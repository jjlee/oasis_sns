package egovframework.oacis.web;

import egovframework.oacis.service.ArticlesService;
import egovframework.oacis.service.BusinessService;
import egovframework.oacis.service.BusinessVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import sun.rmi.runtime.Log;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class SearchController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchController.class);

    @Autowired
    ArticlesService articlesService;

    @Autowired
    BusinessService businessService;

    /*
     * 검색
     */
    @RequestMapping(value = "/search.do")
    public String viewSearch(ModelMap model, HttpServletRequest request) throws Exception {
        String searchWord = request.getParameter("search_word");
        LOGGER.debug(searchWord);
        if (searchWord != null || searchWord != "") {
            List bList = businessService.searchBusiness(searchWord);
            model.addAttribute("bList", bList);
            List aList = articlesService.searchArticles(searchWord);
            model.addAttribute("aList", aList);
        }
        return "/search";
    }

}
