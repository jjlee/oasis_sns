package egovframework.oacis.web;

import egovframework.oacis.service.BusinessVO;
import egovframework.oacis.service.BusinessService;
import egovframework.oacis.service.MemberVO;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

@Controller
public class BusinessController {
    @Autowired
    protected BusinessService businessService;

    @RequestMapping(value = "/business.do")
    public String viewBusiness(@ModelAttribute("BusinessVO") BusinessVO businessVO, ModelMap model){
        List<HashMap> businessList = businessService.selectBusinessList(businessVO);
        int totalCount = businessService.selectBusinessCount();
        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(businessVO.page);
        paginationInfo.setPageSize(businessVO.pageSize);
        paginationInfo.setTotalRecordCount(totalCount);
        paginationInfo.setRecordCountPerPage(businessVO.recordCountPerPage);
        model.addAttribute("paginationInfo", paginationInfo);
        model.addAttribute("businessList", businessList);
        return "/business";
    }

    @RequestMapping(value = "/registBusiness.do")
    public String registBusiness(@ModelAttribute("BusinessVO") BusinessVO businessVO, ModelMap model, HttpServletRequest request){
        if("Y".equals(businessVO.getUpdateYn())){
            int rtn  = businessService.updateBusiness(businessVO);
            if(rtn == 1){
                model.addAttribute("message","success");
            }else{
                model.addAttribute("message","fail");
            }
        }else{
            MemberVO vo = (MemberVO) request.getSession().getAttribute("memberVO");
            if(vo != null){
                businessVO.setCreater(vo.getEmail());
            }
            int rtn = businessService.registBusiness(businessVO);
            if(rtn == 1){
                model.addAttribute("message","success");
            }else{
                model.addAttribute("message","fail");
            }

        }
        return "redirect:/business.do";
    }

    @RequestMapping(value = "/viewRegistBusiness.do")
    public String viewRegistBusiness(@ModelAttribute("BusinessVO") BusinessVO businessVO, ModelMap model){
        if("Y".equals(businessVO.getUpdateYn())){
            HashMap businessData =  businessService.selectBusiness(businessVO);
            businessData.put("updateYn","Y");
            model.addAttribute("businessData", businessData);
        }
        return "/registBusiness";
    }

    @RequestMapping(value = "/viewUpdateBusiness.do")
    public String viewUpdateBusiness(@ModelAttribute("BusinessVO") BusinessVO businessVO, ModelMap model){
        HashMap map = businessService.selectBusiness(businessVO);
        model.addAttribute("business", map);

        List<HashMap> list = businessService.selectReply(businessVO.getNo());
        model.addAttribute("list", list);
        return "/modifyBusiness";
    }

    @RequestMapping(value = "/deleteBusiness.do")
    public String deleteBusiness(@ModelAttribute("BusinessVO") BusinessVO businessVO){
        businessService.deleteBusiness(businessVO);
        return "/business";
    }

    /**
     * [댓글] =========================
     */
    @ResponseBody
    @RequestMapping(value = "/bsnsReply", method = RequestMethod.POST)
    public String businessInsertReply(HttpServletRequest request) {
        HashMap param = new HashMap();
        param.put("mNo", request.getParameter("member_no"));
        param.put("aNo", request.getParameter("article_no"));
        param.put("reply", request.getParameter("article_reply"));
        int insert = businessService.insertReply(param);
        return String.valueOf(insert);
    }

    //// ADMIN ===============
    @RequestMapping("/admin/business")
    public String adminBusiness(@ModelAttribute("BusinessVO") BusinessVO businessVO, ModelMap model) throws Exception {
        List<HashMap> businessList = businessService.selectBusinessList(businessVO);
        int totalCount = businessService.selectBusinessCount();
        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(businessVO.page);
        paginationInfo.setPageSize(businessVO.pageSize);
        paginationInfo.setTotalRecordCount(totalCount);
        paginationInfo.setRecordCountPerPage(businessVO.recordCountPerPage);
        model.addAttribute("paginationInfo", paginationInfo);
        model.addAttribute("businessList", businessList);
        return "/admin/business";
    }


    @RequestMapping("/admin/modifyBusiness")
    public String adminBusinessModify(HttpServletRequest request, BusinessVO businessVO,ModelMap model) throws Exception {
        HashMap businessData =  businessService.selectBusiness(businessVO);
        businessData.put("updateYn","Y");
        model.addAttribute("businessData", businessData);
        return "/admin/modifyBusiness";
    }

    @RequestMapping(value = "/admin/registBusiness.do")
    public String adminRegistBusiness(@ModelAttribute("BusinessVO") BusinessVO businessVO, ModelMap model, HttpServletRequest request){
        if("Y".equals(businessVO.getUpdateYn())){
            int rtn  = businessService.updateBusiness(businessVO);
            if(rtn == 1){
                model.addAttribute("message","success");
            }else{
                model.addAttribute("message","fail");
            }
        }else{
            MemberVO vo = (MemberVO) request.getSession().getAttribute("memberVO");
            if(vo != null){
                businessVO.setCreater(vo.getEmail());
            }
            int rtn = businessService.registBusiness(businessVO);
            if(rtn == 1){
                model.addAttribute("message","success");
            }else{
                model.addAttribute("message","fail");
            }

        }
        return "redirect:/admin/business.do";
    }

    @RequestMapping(value = "/admin/deleteBusiness.do")
    public String adminDeleteBusiness(@ModelAttribute("BusinessVO") BusinessVO businessVO){
        businessService.deleteBusiness(businessVO);
        return "redirect:/admin/business.do";
    }

    @RequestMapping(value = "/admin/viewRegistBusiness.do")
    public String viewAdminRegistBusiness(@ModelAttribute("BusinessVO") BusinessVO businessVO, ModelMap model){
        if("Y".equals(businessVO.getUpdateYn())){
            HashMap businessData =  businessService.selectBusiness(businessVO);
            businessData.put("updateYn","Y");
            model.addAttribute("businessData", businessData);
        }
        return "/admin/addBusiness";
    }
}
