package egovframework.oacis.web;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.hsqldb.lib.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.oacis.service.MainService;

import com.google.gson.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class OacisController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OacisController.class);
	
	/** NoticeService */
	@Autowired
	private MainService mainService;

	/** Validator */
	@Autowired
	protected DefaultBeanValidator beanValidator;
	
	/*
	 * 메인 
	 */
	@RequestMapping(value = "/main.do")
	public String viewMain(ModelMap model) throws Exception {
		LOGGER.debug("========= 메인");
		
		List<HashMap> noticeList = mainService.selectMainNoticeList();
		if ( noticeList != null && noticeList.size() > 0 )
			LOGGER.debug(noticeList.toString());
		model.addAttribute("list", noticeList);
		
		return "/main";
	}

	/*
	 * 사업 발굴 발표회 html
	 */
	@RequestMapping(value = "/bsnsDevtPt")
	public String viewBsnsDevtPt(ModelMap model) throws Exception {
		return "/bsnsDevtPt";
	}

	/*
	 * 교육 및 기타자료 html
	 */
	@RequestMapping(value = "/trnnAndOthrData")
	public String viewTrnnAndOthrData(ModelMap model) throws Exception {
		return "/trnnAndOthrData";
	}

	/*
	 * File Upload
	 */
	@RequestMapping(value = "fileUpload")
	public String fileUpload(HttpServletRequest req, HttpServletResponse resp, MultipartHttpServletRequest multiFile) throws Exception {
		JsonObject json = new JsonObject();
		PrintWriter printWriter = null;
		OutputStream out = null;
		MultipartFile file = multiFile.getFile("upload");
		String originFileName = file.getOriginalFilename();
		String extension = originFileName.split("\\.")[1];
		if (file != null) {
			if (file.getSize() > 0 && StringUtils.isNotBlank(file.getName())) {
				if (file.getContentType().toLowerCase().startsWith("image/")) {
					try {
						String fileName = file.getName();
						byte[] bytes = file.getBytes();
						String uploadPath = req.getServletContext().getRealPath("/img");
						File uploadFile = new File(uploadPath);
						if (!uploadFile.exists()) {
							uploadFile.mkdirs();
						}
						fileName = UUID.randomUUID().toString() + "." + extension;
						uploadPath = uploadPath + "/" + fileName;
						out = new FileOutputStream(new File(uploadPath));
						out.write(bytes);
						printWriter = resp.getWriter();
						String fileUrl = req.getContextPath() + "/img/" + fileName;
						json.addProperty("uploaded", 1);
						json.addProperty("fileName", fileName);
						json.addProperty("url", fileUrl);

						printWriter.println(json);
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
						if (out != null) {
							out.close();
						}
						if (printWriter != null) {
							printWriter.close();
						}
					}
				}
			}
		}
		return null;
	}

}	
