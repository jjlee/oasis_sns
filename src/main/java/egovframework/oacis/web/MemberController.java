package egovframework.oacis.web;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.oacis.service.MemberService;
import egovframework.oacis.service.MemberVO;
import egovframework.oacis.service.TextsService;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

@Controller
public class MemberController {
	private static final Logger LOGGER = LoggerFactory.getLogger(MemberController.class);
	
	@Autowired
	private TextsService textsService;
	
	/** Validator */
	@Autowired
	protected DefaultBeanValidator beanValidator;

	@Autowired
	private MemberService memberService;
	
	@Value("#{snsinfo['clientid.naver']}")
	String clientid_naver;
	@Value("#{snsinfo['clientsecret.naver']}")
	String clientsecret_naver;
	@Value("#{snsinfo['callback.naver']}")
	String callback_naver;
	@Value("#{snsinfo['clientid.kakao']}")
	String clientid_kakao;
	@Value("#{snsinfo['clientsecret.kakao']}")
	String clientsecret_kakao;
	@Value("#{snsinfo['javascriptkey.kakao']}")
	String javascriptkey_kakao;
	@Value("#{snsinfo['nativeappkey.kakao']}")
	String nativeappkey_kakao;
	@Value("#{snsinfo['restapikey.kakao']}")
	String restapikey_kakao;
	@Value("#{snsinfo['adminkey.kakao']}")
	String adminkey_kakao;
	@Value("#{snsinfo['callback.kakao']}")
	String callback_kakao;
	@Value("#{snsinfo['clientid.facebook']}")
	String clientid_facebook;
	@Value("#{snsinfo['clientsecret.facebook']}")
	String clientsecret_facebook;
	@Value("#{snsinfo['callback.facebook']}")
	String callback_facebook;
	/* 
	 * 회원가입 
	 */
	@RequestMapping(value = "/join.do")
	public String viewJoin() throws Exception {
		return "/join";
	}

	@RequestMapping(value = "/registMember.do")
	public String registMember(@ModelAttribute("memberVO") MemberVO memberVO, ModelMap model) throws Exception {
		int rtnVal = 0;
		try {
			rtnVal = memberService.join(memberVO);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("message", "fail");
			return "/join.do";
		}
		if(rtnVal == 1) {
			model.addAttribute("message", "success");
			return "/login";

		}
		return "/join.do";
	}


	/*
	 * 회원가입 약관
	 */
	@RequestMapping(value = "/joinTerm.do")
	public String viewJoinTerm(ModelMap model) throws Exception {
		HashMap contents = textsService.selectTextsContents("JOIN_TERM");
		model.addAttribute("contents", contents);
		return "/joinTerm";
	}
	
	/*
	 * 개인정보보호 약관 
	 */
	@RequestMapping(value = "/privacyTerm.do")
	public String viewPrivacyTerm(ModelMap model) throws Exception {
		HashMap contents = textsService.selectTextsContents("PRIVACY_TERM");
		model.addAttribute("contents", contents);
		return "/privacyTerm";
	}
	
	/*
	 * 로그인
	 */
	@RequestMapping(value = "/login.do")
	public String viewLogin(@ModelAttribute("memberVO") MemberVO memberVO, ModelMap model) throws Exception {
		model.addAttribute("clientid_naver", clientid_naver);
		model.addAttribute("clientsecret_naver", clientsecret_naver);
		model.addAttribute("clientid_kakao", clientid_kakao);
		model.addAttribute("clientsecret_kakao", clientsecret_kakao);
		model.addAttribute("javascriptkey_kakao", javascriptkey_kakao);
		model.addAttribute("nativeappkey_kakao", nativeappkey_kakao);
		model.addAttribute("restapikey_kakao", restapikey_kakao);
		model.addAttribute("adminkey_kakao", adminkey_kakao);
		model.addAttribute("clientid_facebook", clientid_facebook);
		model.addAttribute("clientsecret_facebook", clientsecret_facebook);
		model.addAttribute("callback_naver", callback_naver);
		model.addAttribute("callback_kakao", callback_kakao);
		model.addAttribute("callback_facebook", callback_facebook);
		return "/login";
	}

	/*
	 * 로그인 action
	 */
	@RequestMapping(value = "/actionLogin.do")
	public String actionLogin(@ModelAttribute("memberVO") MemberVO memberVO, HttpServletRequest request, ModelMap model, RedirectAttributes rttr) throws Exception {
		MemberVO vo  = memberService.loginAction(memberVO);
		if (vo != null && !("").equals(vo.getEmail())) {
			request.getSession().setAttribute("memberVO", vo);
			rttr.addFlashAttribute("message","로그인 되었습니다.");
			return "redirect:/main.do";
		} else {
			rttr.addFlashAttribute("message","이메일 또는 패스워드를 확인하세요.");
			return "redirect:/login.do";
		}
	}

	/*
	 * 회원정보 조회 
	 */
	@RequestMapping(value = "/member.do")
	public String viewMember(@ModelAttribute("memberVO") MemberVO memberVO, ModelMap model, HttpServletRequest request) throws Exception {
		MemberVO mVo = (MemberVO) request.getSession().getAttribute("memberVO");
		MemberVO vo = memberService.selectMember(mVo);
		model.addAttribute("memberInfo", vo);
		return "/member";
	}
	
	/*
	 * 마이페이지
	 */
	@RequestMapping(value = "/myPage.do")
	public String viewMyPage(@ModelAttribute("memberVO") MemberVO memberVO, ModelMap model) throws Exception {
		MemberVO vo = memberService.selectMember(memberVO);
		model.addAttribute("memberInfo", vo);
		return "/myPage";
	}
	
	/*
	 * 회원 정보 수정
	 */
	@RequestMapping(value = "/modifyMember.do")
	public String viewModifyMember(@ModelAttribute("memberVO") MemberVO memberVO, ModelMap model) throws Exception {
		int result = memberService.modifyMember(memberVO);
		return "/modifyMember";
	}
	
	/*
	 * 비밀번호 변경
	 */
	@RequestMapping(value = "/modifyPwd.do")
	public String viewModifyPwd(@ModelAttribute("memberVO") MemberVO memberVO, ModelMap model) throws Exception {
		int result = memberService.modifyMember(memberVO);
		return "/modifyPwd";
	}
	
	/*
	 * 비밀번호 재발급
	 */
	@RequestMapping(value = "/rePwd.do")
	public String viewRePwd(@ModelAttribute("memberVO") MemberVO memberVO, ModelMap model) throws Exception {
		int result = memberService.modifyMember(memberVO);
		return "/repassword";
	}

	/*
	 * 로그아웃 action
	 */
	@RequestMapping(value = "/logout.do")
	public String actionLogout(ModelMap model, HttpServletRequest request, RedirectAttributes rttr) throws Exception {
		request.getSession().setAttribute("memberVO", null);
		rttr.addFlashAttribute("message","로그아웃 되었습니다.");
		return "redirect:/main.do";
	}

	/**
	 * [ADMIN] =========================================================
	 */
	@RequestMapping("/admin/member")
	public String adminMember(ModelMap model
			, @ModelAttribute("memberVO") MemberVO memberVO) throws Exception {

		int totalCount = memberService.selectMemberCount();

		List list = memberService.selectMemberList(memberVO);
		model.addAttribute("list", list);

		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(memberVO.page);
		paginationInfo.setPageSize(memberVO.pageSize);
		paginationInfo.setTotalRecordCount(totalCount);
		paginationInfo.setRecordCountPerPage(memberVO.recordCountPerPage);
		model.addAttribute("paginationInfo", paginationInfo);
		return "admin/member/member";
	}
}
