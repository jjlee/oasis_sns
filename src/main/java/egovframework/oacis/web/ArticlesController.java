package egovframework.oacis.web;

import egovframework.oacis.service.ArticlesService;
import egovframework.oacis.service.ArticlesVO;
import egovframework.oacis.service.FinanceVO;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.awt.print.Pageable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ArticlesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArticlesController.class);

    @Autowired
    private ArticlesService articlesService;

    /**
     *  [게시판] 목록
     */
    @RequestMapping(value = "/board.do")
    public String viewBoard(ModelMap model, @ModelAttribute("articlesVO") ArticlesVO articlesVO) throws Exception {
        if (articlesVO.category == null) {
            articlesVO.category = "COMM_01";
        }
        HashMap map = articlesService.selectCategoryName(articlesVO);
//        LOGGER.debug(map.toString());
        model.addAttribute("catInfo", map);
        List<HashMap> list = articlesService.selectList(articlesVO);
        model.addAttribute("list", list);
//        LOGGER.debug(String.valueOf(articlesVO.page));
        int totalCount = articlesService.selectArticlesCount(articlesVO);

        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(articlesVO.page);
        paginationInfo.setPageSize(articlesVO.pageSize);
        paginationInfo.setTotalRecordCount(totalCount);
        paginationInfo.setRecordCountPerPage(articlesVO.recordCountPerPage);
        model.addAttribute("paginationInfo", paginationInfo);
        return "/board";
    }

    /**
     *  [게시판] 상세
     */
    @RequestMapping(value = "/boardDetail.do")
    public String viewBoardDetail(ModelMap model, @ModelAttribute("articlesVO") ArticlesVO articlesVO) throws Exception {
        try {
            HashMap map = articlesService.selectCategoryName(articlesVO);
//        LOGGER.debug(map.toString());
            model.addAttribute("catInfo", map);
            HashMap article = articlesService.selectDetail(articlesVO);
            model.addAttribute("article", article);

            List list = articlesService.selectReply(articlesVO.getNo());
            model.addAttribute("list", list);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return "/boardDetail";
    }

    /**
     *  [게시판] 글 작성
     */
    @RequestMapping(value = "/boardWrite.do")
    public String viewBoardWrite(ModelMap model, @ModelAttribute("articlesVO") ArticlesVO articlesVO) throws Exception {
        HashMap map = articlesService.selectCategoryName(articlesVO);
        model.addAttribute("catInfo", map);
        return "/boardWrite";
    }

    /**
     * [게시판] 글 생성
     * */
    @RequestMapping(value = "/actionBoardWrite.do")
    public String actionBoardWrite(ModelMap model, @ModelAttribute("articlesVO") ArticlesVO articlesVO) throws Exception {
        int result = articlesService.insertArticles(articlesVO);
        LOGGER.debug(String.valueOf(result));
        if (result > 0) {
            // TODO: 결과 처리
            model.addAttribute("articlesVO", articlesVO);
            return "redirect:/board.do";
        }
        else {

        }
        return "/board";
    }

    @RequestMapping(value = "/modifyBoard.do")
    public String viewModifyBoard(ModelMap model
            , @ModelAttribute("articlesVO") ArticlesVO articlesVO) throws Exception {
        HashMap article = articlesService.selectDetail(articlesVO);
        model.addAttribute("article", article);
        return "modifyBoard";
    }

    @RequestMapping(value = "/actionModifyBoard.do", method = RequestMethod.POST)
    public String actionModifyBoard(ModelMap model,
                                    @ModelAttribute("articlesVO") ArticlesVO articlesVO) throws Exception {
        int result = articlesService.updateArticles(articlesVO);
        if (result == 1) {
            try {
                HashMap map = articlesService.selectCategoryName(articlesVO);
//        LOGGER.debug(map.toString());
                model.addAttribute("catInfo", map);
                HashMap article = articlesService.selectDetail(articlesVO);
                model.addAttribute("article", article);

                List list = articlesService.selectReply(articlesVO.getNo());
                model.addAttribute("list", list);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return "redirect:/boardDetail.do";
        }
        return "";
    }

    /**
     * [게시판] 댓글 작성
     * */
    @ResponseBody
    @RequestMapping(value = "/actionArticleReply.do", method = RequestMethod.POST)
    public String actionArticleReply(HttpServletRequest request) throws Exception {

        HashMap param = new HashMap();
        param.put("mNo", request.getParameter("member_no"));
        param.put("aNo", request.getParameter("article_no"));
        param.put("reply", request.getParameter("article_reply"));
        int result = articlesService.insertReply(param);
        LOGGER.debug(String.valueOf(result));
        return String.valueOf(result);
    }




    /**
     * [ADMIN] =========================================================
     */
    @RequestMapping("/admin/board")
    public String adminBoard(ModelMap model, @ModelAttribute("articlesVO") ArticlesVO articlesVO) throws Exception {
        if (articlesVO.category == null) {
            articlesVO.category = "NOTICE";
        }

        List categoryList = articlesService.selectArticleCategoryList();
        model.addAttribute("cList", categoryList);

        int totalCount = articlesService.selectBoardAllCount(articlesVO);

        PaginationInfo paginationInfo = new PaginationInfo();
        paginationInfo.setCurrentPageNo(articlesVO.page);
        paginationInfo.setPageSize(articlesVO.pageSize);
        paginationInfo.setTotalRecordCount(totalCount);
        paginationInfo.setRecordCountPerPage(articlesVO.recordCountPerPage);
        model.addAttribute("paginationInfo", paginationInfo);

        List list = articlesService.selectBoardAllList(articlesVO);
        model.addAttribute("list", list);
        return "admin/board/board";
    }

    @RequestMapping(value = "/admin/modifyBoard", method = RequestMethod.POST)
    public String adminModifyBoard(ModelMap model, @ModelAttribute("articlesVO") ArticlesVO articlesVO) throws Exception {
        HashMap article = articlesService.selectDetail(articlesVO);
        model.addAttribute("article", article);
        return "admin/board/modifyBoard";
    }

    @RequestMapping(value = "/admin/actionModifyBoard.do", method = RequestMethod.POST)
    public String actionAdminModifyBoard(ModelMap model,
                                    @ModelAttribute("articlesVO") ArticlesVO articlesVO) throws Exception {
        int result = articlesService.updateArticles(articlesVO);
        if (result == 1) {
            try {
                HashMap map = articlesService.selectCategoryName(articlesVO);
//        LOGGER.debug(map.toString());
                model.addAttribute("catInfo", map);
                HashMap article = articlesService.selectDetail(articlesVO);
                model.addAttribute("article", article);

                List list = articlesService.selectReply(articlesVO.getNo());
                model.addAttribute("list", list);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return "redirect:admin/board.do";
        }
        return "";
    }

    @RequestMapping("/admin/community")
    public String adminCommunity(ModelMap model) throws Exception {
        List<HashMap> list = articlesService.selectAllCategory();
        model.addAttribute("list", list);
        return "admin/community/community";
    }

    @RequestMapping("/admin/modifyCommunity")
    public String adminCommunityModify(
            ModelMap model
            , HttpServletRequest request
            , @RequestParam Map<String, Object> param) throws Exception {
        if (param.size() > 0) {
            int no = Integer.parseInt(param.get("board_no").toString());
            HashMap map = articlesService.selectCategory(no);
            model.addAttribute("c", map);
        }
        return "/admin/modifyCommunity";
    }

    @RequestMapping("/admin/actionCommunity")
    public String adminCommunityAction(@RequestParam Map<String, Object> param) throws Exception {

        return "admin/community/community";
    }

    @ResponseBody
    @RequestMapping("/admin/deleteArticle")
    public String adminDeleteArticle(ArticlesVO vo) throws Exception {
        int result = articlesService.deleteArticle(vo);
        return String.valueOf(result);
    }

    @ResponseBody
    @RequestMapping("/admin/showArticle")
    public String adminShowArticle(ArticlesVO vo) throws Exception {
        vo.blind = 0;
        int result = articlesService.blindArticle(vo);
        return String.valueOf(result);
    }

    @ResponseBody
    @RequestMapping("/admin/hideArticle")
    public String adminHideArticle(ArticlesVO vo) throws Exception {
        vo.blind = 1;
        int result = articlesService.blindArticle(vo);
        return String.valueOf(result);
    }

}
