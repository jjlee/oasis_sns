/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package egovframework.oacis.cmmn.web;

import egovframework.rte.ptl.mvc.tags.ui.pagination.AbstractPaginationRenderer;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

import java.text.MessageFormat;

public class OacisPaginationRenderer extends AbstractPaginationRenderer {

	public OacisPaginationRenderer() {
		firstPageLabel = "<a href=\"javascript:{0}({1});\">처음</a> ";
		previousPageLabel = "<a href=\"javascript:{0}({1});\">이전</a> ";
		currentPageLabel = "{0}/{1} ";
		nextPageLabel = "<a href=\"javascript:{0}({1});\">다음</a> ";
		lastPageLabel = "<a href=\"javascript:{0}({1});\">맨끝</a>";
	}

	@Override
	public String renderPagination(PaginationInfo paginationInfo, String jsFunction) {
		StringBuffer strBuff = new StringBuffer();

		int firstPageNo = paginationInfo.getFirstPageNo();
		int firstPageNoOnPageList = paginationInfo.getFirstPageNoOnPageList();
		int totalPageCount = paginationInfo.getTotalPageCount();
		int pageSize = paginationInfo.getPageSize();
		int lastPageNoOnPageList = paginationInfo.getLastPageNoOnPageList();
		int currentPageNo = paginationInfo.getCurrentPageNo();
		int lastPageNo = paginationInfo.getLastPageNo();

		/// 현재 페이지가 첫번째 페이지 보다 클때
		if (currentPageNo > firstPageNo) {
			strBuff.append(MessageFormat.format(firstPageLabel, jsFunction, Integer.toString(firstPageNo)));
			strBuff.append(MessageFormat.format(previousPageLabel, jsFunction, Integer.toString(currentPageNo - 1)));
		}
		strBuff.append(MessageFormat.format(currentPageLabel, Integer.toString(currentPageNo), Integer.toString(totalPageCount)));
		/// 현재 페이지가 마지막 페이지보다 작을때
		if (currentPageNo < lastPageNo) {
			strBuff.append(MessageFormat.format(nextPageLabel, jsFunction, Integer.toString(currentPageNo + 1)));
			strBuff.append(MessageFormat.format(lastPageLabel, jsFunction, Integer.toString(lastPageNo)));
		}

		return strBuff.toString();
	}
}
