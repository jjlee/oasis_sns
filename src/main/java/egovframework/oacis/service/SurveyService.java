package egovframework.oacis.service;

import java.util.HashMap;
import java.util.List;

public interface SurveyService {
    int selectSurveyCount();

    List<HashMap> selectSurveyList(SurveyVO vo);

    HashMap selectSurveyDetail(SurveyVO vo);

    List<HashMap> selectSurveyDetailQuestions(SurveyVO vo);

    List<HashMap> selectSurveyDetailAnswers(int no);

    public int selectSurveyAllCount();

    public List<HashMap> selectSurveyAllList(SurveyVO vo);

    public int deleteSurvey(SurveyVO vo);
    public int showSurvey(SurveyVO vo);
}
