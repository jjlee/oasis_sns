package egovframework.oacis.service;

import java.util.HashMap;
import java.util.List;

/**
 * 공지사항 관련 서비스
 * @author Jinuk
 *
 */
public interface ArticlesService {

	int selectArticlesCount(ArticlesVO vo);

	HashMap selectCategoryName(ArticlesVO vo);

	List<HashMap> selectList(ArticlesVO vo);
	
	HashMap selectDetail(ArticlesVO vo);

	int insertArticles(ArticlesVO vo);

	int updateArticles(ArticlesVO vo);

	List<HashMap> selectAllCategory();

	HashMap selectCategory(int no);

	public List<HashMap> selectReply(int no);

	public int insertReply(HashMap param);

	public List<HashMap> searchArticles(String searchWord);

	public List<HashMap> selectArticleCategoryList();

	public List<HashMap> selectBoardAllList(ArticlesVO vo);

	public int selectBoardAllCount(ArticlesVO vo);

	public int deleteArticle(ArticlesVO vo);

	public int blindArticle(ArticlesVO vo);
}