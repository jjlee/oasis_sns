package egovframework.oacis.service.impl;

import egovframework.oacis.service.SurveyVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository("surveyDAO")
public class SurveyDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(SurveyDAO.class);

    @Autowired
    private SqlSessionTemplate sqlSession;

    public int selectSurveyCount() {
        return sqlSession.selectOne("selectSurveyCount");
    }

    public List<HashMap> selectSurveyList(SurveyVO vo) {
        return sqlSession.selectList("selectSurveyList", vo);
    }

    public HashMap selectSurveyDetail(SurveyVO vo) {
        return sqlSession.selectOne("selectSurveyDetail", vo);
    }

    public List<HashMap> selectSurveyDetailQuestions(SurveyVO vo) {
        return sqlSession.selectList("selectSurveyDetailQuestions", vo);
    }

    public List<HashMap> selectSurveyDetailAnswers(int no) {
        return sqlSession.selectList("selectSurveyDetailAnswers", no);
    }

    public int selectSurveyAllCount() {
        return sqlSession.selectOne("selectSurveyAllCount");
    }

    public List<HashMap> selectSurveyAllList(SurveyVO vo) {
        return sqlSession.selectList("selectSurveyAllList", vo);
    }

    public int deleteSurvey(SurveyVO vo){
        return sqlSession.delete("deleteSurvey", vo);
    }

    public int showSurvey(SurveyVO vo) {
        return sqlSession.update("showSurvey", vo);
    }

    public List<HashMap> selectResponseSurvey(HashMap param) {
        return sqlSession.selectList("selectResponseSurvey", param);
    }

}
