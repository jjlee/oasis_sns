package egovframework.oacis.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.oacis.service.ArticlesService;
import egovframework.oacis.service.ArticlesVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("articlesService")
public class ArticlesServiceImpl extends EgovAbstractServiceImpl implements ArticlesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ArticlesServiceImpl.class);

	/**
	 * Articles DAO
	 */
	@Resource(name = "articlesDAO")
	private ArticlesDAO articlesDAO;

	@Override
	public int selectArticlesCount(ArticlesVO vo) {
		return articlesDAO.selectArticleCount(vo);
	}

	public HashMap selectCategoryName(ArticlesVO vo) {
		HashMap map = null;
		try {
			map = articlesDAO.selectCategoryName(vo);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public List<HashMap> selectList(ArticlesVO vo) {
		List<HashMap> list = null;
		try {
			list = articlesDAO.selectList(vo);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public HashMap selectDetail(ArticlesVO vo) {
		HashMap map = null;
		try {
			map = articlesDAO.selectOne(vo);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	@Override
	public int insertArticles(ArticlesVO vo) {
		return articlesDAO.insertArticle(vo);
	}

	@Override
	public int updateArticles(ArticlesVO vo) {
		return articlesDAO.updateArticles(vo);
	}

	@Override
	public List<HashMap> selectAllCategory() {
		List list = null;
		try {
			list = articlesDAO.selectAllCategory();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public HashMap selectCategory(int no) {
		HashMap map = null;
		try {
			map = articlesDAO.selectCategory(no);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	@Override
	public List<HashMap> selectReply(int no) {
		return articlesDAO.selectReply(no);
	}

	public int insertReply(HashMap param) { return articlesDAO.insertReply(param); }

	@Override
	public List<HashMap> searchArticles(String searchWord) {
		return articlesDAO.searchArticles(searchWord);
	}

	@Override
	public List<HashMap> selectArticleCategoryList() {
		return articlesDAO.selectArticleCategoryList();
	}

	@Override
	public List<HashMap> selectBoardAllList(ArticlesVO vo) {
		return articlesDAO.selectBoardAllList(vo);
	}

	@Override
	public int selectBoardAllCount(ArticlesVO vo) {
		return articlesDAO.selectBoardAllCount(vo);
	}

	@Override
	public int deleteArticle(ArticlesVO vo) {
		return articlesDAO.deleteArticle(vo);
	}

	@Override
	public int blindArticle(ArticlesVO vo) {
		return articlesDAO.blindArticle(vo);
	}

}
