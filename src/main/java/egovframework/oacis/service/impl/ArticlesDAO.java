package egovframework.oacis.service.impl;

import java.util.HashMap;
import java.util.List;

import com.mysql.cj.PreparedQuery;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import egovframework.oacis.service.ArticlesVO;

/**
 * @author Jinuk
 * `ARTICLES` DAO
 */
@Repository("articlesDAO")
public class ArticlesDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ArticlesDAO.class);
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	/**
	 * [메인] 공지사항 가져오기
	 * @return
	 * @throws Exception
	 */
	public List<HashMap> selectMainNoticeLatest() {
		return sqlSession.selectList("egovframework.sqlmap.oacis.mainSql.main_selectNoticeLatest");
	}

	/**
	 * [게시판] 카테고리 정보 가져오기
	 * */
	public HashMap selectCategoryName(ArticlesVO vo) {
		return sqlSession.selectOne("egovframework.sqlmap.oacis.articlesSql.selectCategoryName", vo);
	}

	/**
	 * [게시판] 해당 카테고리 총 개시물 개수 가져오기
	 * */
	public int selectArticleCount(ArticlesVO vo) {
		return sqlSession.selectOne("egovframework.sqlmap.oacis.articlesSql.selectArticlesCount", vo);
	}

	/**
	 * [게시판] 목록 가져오기
	 * @return
	 * @throws Exception
	 */
	public List<HashMap> selectList(ArticlesVO vo) {
		return sqlSession.selectList("egovframework.sqlmap.oacis.articlesSql.selectBoardList", vo);
	}
	
	/**
	 * [게시판] 상세 가져오기
	 * @return
	 * @throws Exception
	 */
	public HashMap selectOne(ArticlesVO vo) {
		return sqlSession.selectOne("egovframework.sqlmap.oacis.articlesSql.selectBoardDetail", vo);
	}

	/**
	 * 글 입력
	 * */
	public int insertArticle(ArticlesVO vo) {
		return sqlSession.insert("egovframework.sqlmap.oacis.articlesSql.insertArticles", vo);
	}

	/**
	 * 글 수정
	 * */
	public int updateArticles(ArticlesVO vo) {
		return sqlSession.update("egovframework.sqlmap.oacis.articlesSql.updateArticles", vo);
	}

	/**
	 * [ADMIN] =========================================================
	 */

	/**
	 * 공지제외 카테고리
	 * */
	public List<HashMap> selectAllCategory() {
		return sqlSession.selectList("egovframework.sqlmap.oacis.articlesSql.selectCommunity");
	}

	public HashMap selectCategory(int no) {
		return sqlSession.selectOne("egovframework.sqlmap.oacis.articlesSql.selectCommunity", no);
	}

	public int insertReply(HashMap param) {
		return sqlSession.insert("egovframework.sqlmap.oacis.articlesSql.insertReply", param);
	}

	public List<HashMap> selectReply(int no) {
		return sqlSession.selectList("egovframework.sqlmap.oacis.articlesSql.selectReply", no);
	}

	/// 검색 결과
	public List<HashMap> searchArticles(String searchWord) {
		return sqlSession.selectList("egovframework.sqlmap.oacis.articlesSql.searchArticle", searchWord);
	}

	public List<HashMap> selectArticleCategoryList() {
		return sqlSession.selectList("selectArticleCategoryList");
	}

	public int selectBoardAllCount(ArticlesVO vo) {
		return sqlSession.selectOne("selectBoardAllCount", vo);
	}

	public List<HashMap> selectBoardAllList(ArticlesVO vo) {
		return sqlSession.selectList("selectBoardAllList", vo);
	}

	public int deleteArticle(ArticlesVO vo) {
		return sqlSession.delete("deleteArticle", vo);
	}

	public int blindArticle(ArticlesVO vo) {
		return sqlSession.update("blindArticle", vo);
	}
}
