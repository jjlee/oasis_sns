package egovframework.oacis.service.impl;

import egovframework.oacis.service.FinanceService;
import egovframework.oacis.service.FinanceVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;


@Service("financeService")
public class FinanceServiceImpl implements FinanceService {

    @Resource(name = "financeDAO")
    private FinanceDAO financeDAO;
    
    @Override
    public int registFinance(FinanceVO financeVO) {
        return financeDAO.registFinance(financeVO);
    }

    @Override
    public int updateFinance(FinanceVO financeVO) {
        return financeDAO.updateFinance(financeVO);
    }

    @Override
    public int deleteFinance(FinanceVO financeVO) {
        return financeDAO.deleteFinance(financeVO);
    }

    @Override
    public List<HashMap> selectFinanceList(FinanceVO financeVO) {
        return financeDAO.selectFinanceList(financeVO);
    }

    @Override
    public HashMap selectFinance(FinanceVO financeVO) {
        return financeDAO.selectFinance(financeVO);
    }

    @Override
    public int selectFinanceCount() {
        return financeDAO.selectFinanceCount();
    }
}
