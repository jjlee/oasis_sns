package egovframework.oacis.service.impl;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("textsDAO")
public class TextsDAO  {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TextsDAO.class);
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	/**
	 * 목록 가져오기
	 */
	public List<HashMap> selectAllTexts() {
		return sqlSession.selectList("egovframework.sqlmap.oacis.textsSql.selectAllTexts");
	}

	/**
	 * `TEXTS` 내용 가져오기
	 * @return
	 * @throws Exception
	 */
	public HashMap selectText(String title) {
		return sqlSession.selectOne("egovframework.sqlmap.oacis.textsSql.selectTexts", title);
	}

	public void updateText(HashMap map) {
		sqlSession.update("egovframework.sqlmap.oacis.textsSql.updateTexts", map);
	}
}
