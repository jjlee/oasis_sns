package egovframework.oacis.service.impl;

import egovframework.oacis.service.SurveyService;
import egovframework.oacis.service.SurveyVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@Service("surveyService")
public class SurveyServiceImpl extends EgovAbstractServiceImpl implements SurveyService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SurveyServiceImpl.class);

    @Resource(name = "surveyDAO")
    private SurveyDAO surveyDAO;


    @Override
    public int selectSurveyCount() {
        return surveyDAO.selectSurveyCount();
    }

    @Override
    public List<HashMap> selectSurveyList(SurveyVO vo) {
        return surveyDAO.selectSurveyList(vo);
    }

    @Override
    public HashMap selectSurveyDetail(SurveyVO vo) {
        return surveyDAO.selectSurveyDetail(vo);
    }

    @Override
    public List<HashMap> selectSurveyDetailQuestions(SurveyVO vo) {
        return surveyDAO.selectSurveyDetailQuestions(vo);
    }

    @Override
    public List<HashMap> selectSurveyDetailAnswers(int no) {
        return surveyDAO.selectSurveyDetailAnswers(no);
    }

    @Override
    public int selectSurveyAllCount() {
        return surveyDAO.selectSurveyAllCount();
    }

    @Override
    public List<HashMap> selectSurveyAllList(SurveyVO vo) {
        return surveyDAO.selectSurveyAllList(vo);
    }

    @Override
    public int deleteSurvey(SurveyVO vo) {
        return surveyDAO.deleteSurvey(vo);
    }

    @Override
    public int showSurvey(SurveyVO vo) {
        return surveyDAO.showSurvey(vo);
    }
}
