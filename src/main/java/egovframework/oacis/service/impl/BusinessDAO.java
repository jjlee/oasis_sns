package egovframework.oacis.service.impl;

import egovframework.oacis.service.BusinessVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;


@Repository("businessDAO")
public class BusinessDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArticlesDAO.class);
    @Autowired
    private SqlSessionTemplate sqlSession;

    public int registBusiness(BusinessVO businessVO){
        return sqlSession.insert("insertBusiness" , businessVO);
    }

    public int updateBusiness(BusinessVO businessVO){
        return sqlSession.update("updateBusiness", businessVO);
    }

    public int deleteBusiness(BusinessVO businessVO){
        return sqlSession.delete("deleteBusiness", businessVO);
    }

    public List<HashMap> selectBusinessList(BusinessVO businessVO){
        return sqlSession.selectList("selectBusinessList", businessVO);
    }

    public HashMap selectBusiness(BusinessVO businessVO){
        return sqlSession.selectOne("selectBusiness", businessVO);
    }

    public int selectBusinessCount(){
        return sqlSession.selectOne("selectBusinessCount");
    }

    public List<HashMap> selectReply(int no) {
        return sqlSession.selectList("selectBusinessReply", no);
    }

    /* 댓글 쓰기 */
    public int insertReply(HashMap param) {
        return sqlSession.insert("insertBusinessReply", param);
    }

    public List<HashMap> searchBusiness(String searchWord) {
        return sqlSession.selectList("searchBusiness", searchWord);
    }
}
