package egovframework.oacis.service.impl;

import egovframework.oacis.service.FinanceVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository("financeDAO")
public class FinanceDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArticlesDAO.class);
    @Autowired
    private SqlSessionTemplate sqlSession;

    public int registFinance(FinanceVO financeVO){
        return sqlSession.insert("insertFinance" , financeVO);
    }

    public int updateFinance(FinanceVO financeVO){
        return sqlSession.update("updateFinance", financeVO);
    }

    public int deleteFinance(FinanceVO financeVO){
        return sqlSession.delete("deleteFinance", financeVO);
    }

    public List<HashMap> selectFinanceList(FinanceVO financeVO){
        return sqlSession.selectList("selectFinanceList", financeVO);
    }

    public HashMap selectFinance(FinanceVO financeVO){
        return sqlSession.selectOne("selectFinance", financeVO);
    }

    public int selectFinanceCount(){
        return sqlSession.selectOne("selectFinanceCount");
    }
}
