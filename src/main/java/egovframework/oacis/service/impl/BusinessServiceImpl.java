package egovframework.oacis.service.impl;

import egovframework.oacis.service.BusinessVO;
import egovframework.oacis.service.BusinessService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@Service("businessService")
public class BusinessServiceImpl implements BusinessService {

    @Resource(name = "businessDAO")
    private BusinessDAO businessDAO;

    @Override
    public int registBusiness(BusinessVO businessVO) {
        return businessDAO.registBusiness(businessVO);
    }

    @Override
    public int updateBusiness(BusinessVO businessVO) {
        return businessDAO.updateBusiness(businessVO);
    }

    @Override
    public int deleteBusiness(BusinessVO businessVO) {
        return businessDAO.deleteBusiness(businessVO);
    }

    @Override
    public List<HashMap> selectBusinessList(BusinessVO businessVO) {
        return businessDAO.selectBusinessList(businessVO);
    }

    @Override
    public HashMap selectBusiness(BusinessVO businessVO) {
        return businessDAO.selectBusiness(businessVO);
    }

    @Override
    public int selectBusinessCount() {
        return businessDAO.selectBusinessCount();
    }

    @Override
    public int insertReply(HashMap param) {
        return businessDAO.insertReply(param);
    }

    @Override
    public List<HashMap> selectReply(int no) {
        return businessDAO.selectReply(no);
    }

    @Override
    public List<HashMap> searchBusiness(String searchWord) {
        return businessDAO.searchBusiness(searchWord);
    }
}
