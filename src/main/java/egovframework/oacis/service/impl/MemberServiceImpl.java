package egovframework.oacis.service.impl;

import egovframework.oacis.service.MemberVO;
import egovframework.oacis.service.MemberService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Member;
import java.util.HashMap;
import java.util.List;

@Service("memberService")
public class MemberServiceImpl implements MemberService {

    @Resource(name = "memberDAO")
    MemberDAO memberDAO;

    @Override
    public int join(MemberVO memberVO) throws Exception {
        return memberDAO.join(memberVO);
    }

    @Override
    public MemberVO loginAction(MemberVO memberVO) throws Exception {
        return memberDAO.loginAction(memberVO);
    }

    @Override
    public MemberVO selectMember(MemberVO memberVO) throws Exception {
        return memberDAO.selectMember(memberVO);
    }

    @Override
    public int modifyMember(MemberVO memberVO) throws Exception {
        return memberDAO.modifyMember(memberVO);
    }

    @Override
    public int modifyPwd(MemberVO memberVO) throws Exception {
        return memberDAO.modifyMember(memberVO);
    }

    @Override
    public int rePwd(MemberVO memberVO) throws Exception {
        return memberDAO.modifyMember(memberVO);
    }

    @Override
    public List<HashMap> selectMemberList(MemberVO vo) {
        return memberDAO.selectMemberList(vo);
    }

    @Override
    public int selectMemberCount() {
        return memberDAO.selectMemberCount();
    }
}
