package egovframework.oacis.service.impl;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.oacis.service.TextsService;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

import java.util.HashMap;
import java.util.List;

@Service("textsService")
public class TextsServiceImpl extends EgovAbstractServiceImpl implements TextsService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MainServiceImpl.class);

	@Resource(name = "textsDAO")
	private TextsDAO textsDAO;

	@Override
	public List<HashMap> selectTextsList() {
		List list = null;
		try {
			list = textsDAO.selectAllTexts();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public HashMap selectTextsContents(String title) {
		HashMap contents = null;
		try {
			contents = textsDAO.selectText(title);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contents;
	}

	@Override
	public void updateTextContents(HashMap map) {
		try {
			textsDAO.updateText(map);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
