package egovframework.oacis.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import egovframework.oacis.service.MainService;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

@Service("mainService")
public class MainServiceImpl extends EgovAbstractServiceImpl implements MainService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MainServiceImpl.class);
	
	/**
	 *
	 */
	@Resource(name = "articlesDAO")
	private ArticlesDAO articlesDAO;
	
	/**
	 * [메인] 공지사항 리스트 가져오기
	 * @return ArticleVO 게시글 중 공지
	 * @throws Exception
	 */
	public List<HashMap> selectMainNoticeList() {
		LOGGER.debug("==========");
		List<HashMap> retList = null;
		try {
			retList = articlesDAO.selectMainNoticeLatest();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retList; 
	}
}
