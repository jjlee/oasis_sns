package egovframework.oacis.service.impl;

import egovframework.oacis.service.MemberVO;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository("memberDAO")
public class MemberDAO {
    @Autowired
    private SqlSessionTemplate sqlSession;

    public int join(MemberVO memberVO){
        return sqlSession.insert("insertMemberInfo", memberVO);
    }

    public MemberVO loginAction(MemberVO memberVO){

        return sqlSession.selectOne("selectActionLogin", memberVO);
    }

    public MemberVO selectMember(MemberVO memberVO) {
        return  sqlSession.selectOne("selectMemberInfo", memberVO);
    }


    public int modifyMember(MemberVO memberVO){
        return sqlSession.update("updateMemberInfo",memberVO);
    }

    public int modifyPwd(MemberVO memberVO) {
        return sqlSession.update("updateMemberInfo",memberVO);
    }

    public int rePwd(MemberVO memberVO) {
        return sqlSession.update("updateMemberInfo",memberVO);
    }

    public List<HashMap> selectMemberList(MemberVO vo) {
        return sqlSession.selectList("selectMemberList", vo);
    }

    public int selectMemberCount() {
        return sqlSession.selectOne("selectMemberCount");
    }
}
