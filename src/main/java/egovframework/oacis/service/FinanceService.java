package egovframework.oacis.service;

import java.util.HashMap;
import java.util.List;

public interface FinanceService {
    int registFinance(FinanceVO financeVO);
    int updateFinance(FinanceVO financeVO);
    int deleteFinance(FinanceVO financeVO);
    List<HashMap> selectFinanceList(FinanceVO financeVO);
    HashMap selectFinance(FinanceVO financeVO);
    int selectFinanceCount();
}
