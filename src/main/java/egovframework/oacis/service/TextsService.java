package egovframework.oacis.service;

import java.util.HashMap;
import java.util.List;

public interface TextsService {

	List<HashMap> selectTextsList();

	/** 
	 * `TEXTS` Table의 `TITLE`에 따른 `BODY`를 가져옴
	 * @param title `TEXTS.TITLE` 타이틀
	 * @return String
	 * @throws Exception
	 */
    HashMap selectTextsContents(String title);

    void updateTextContents(HashMap map);
}
