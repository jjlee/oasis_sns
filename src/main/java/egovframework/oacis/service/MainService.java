package egovframework.oacis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 메인 관련 서비스
 * @author Jinuk
 *
 */
public interface MainService {
	
	/**
	 * [메인] 공지사항 리스트
	 * @return
	 * @throws Exception
	 */
	List<HashMap> selectMainNoticeList();
	
}