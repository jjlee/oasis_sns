package egovframework.oacis.service;

import java.util.Date;

public class FinanceVO extends PagingVO {
    private int no;
    private String fName;
    private String fType;
    private String fZip;
    private String fAddr;
    private String fChargerDesc;
    private String fService;
    private String fChargerName;
    private String fChargerContact;
    private String fChargerEmail;
    private Date saved;
    private Date updated;
    private String fServiceDetail;
    private String creater;
    private String updateYn;

    public String getfAddr() {
        return fAddr;
    }

    public void setfAddr(String fAddr) {
        this.fAddr = fAddr;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public String getUpdateYn() {
        return updateYn;
    }

    public void setUpdateYn(String updateYn) {
        this.updateYn = updateYn;
    }

    public String getfServiceDetail() {
        return fServiceDetail;
    }

    public void setfServiceDetail(String fServiceDetail) {
        this.fServiceDetail = fServiceDetail;
    }

    public String getfChargerDesc() {
        return fChargerDesc;
    }

    public void setfChargerDesc(String fChargerDesc) {
        this.fChargerDesc = fChargerDesc;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getfType() {
        return fType;
    }

    public void setfType(String fType) {
        this.fType = fType;
    }

    public String getfZip() {
        return fZip;
    }

    public void setfZip(String fZip) {
        this.fZip = fZip;
    }

    public String getfService() {
        return fService;
    }

    public void setfService(String fService) {
        this.fService = fService;
    }

    public String getfChargerName() {
        return fChargerName;
    }

    public void setfChargerName(String fChargerName) {
        this.fChargerName = fChargerName;
    }

    public String getfChargerContact() {
        return fChargerContact;
    }

    public void setfChargerContact(String fChargerContact) {
        this.fChargerContact = fChargerContact;
    }

    public String getfChargerEmail() {
        return fChargerEmail;
    }

    public void setfChargerEmail(String fChargerEmail) {
        this.fChargerEmail = fChargerEmail;
    }

    public Date getSaved() {
        return saved;
    }

    public void setSaved(Date saved) {
        this.saved = saved;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
