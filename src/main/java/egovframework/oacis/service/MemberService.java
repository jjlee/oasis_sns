package egovframework.oacis.service;

import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.HashMap;
import java.util.List;

public interface MemberService {
    int join(MemberVO memberVO) throws Exception;
    MemberVO loginAction(MemberVO memberVO) throws Exception;
    MemberVO selectMember(MemberVO memberVO) throws Exception;
    int modifyMember(MemberVO memberVO) throws Exception;
    int modifyPwd(MemberVO memberVO) throws Exception;
    int rePwd(MemberVO memberVO) throws Exception;

    List<HashMap> selectMemberList(MemberVO vo);
    int selectMemberCount();
}