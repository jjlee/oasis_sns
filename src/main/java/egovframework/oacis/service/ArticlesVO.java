package egovframework.oacis.service;

import java.util.Date;

/**
 * @author Z-Nuke 
 * 게시판 VO
 */
public class ArticlesVO extends PagingVO {

	/**
	 * 게시물 일련번호
	 */
	public int no;

	/**
	 * 작성자 회원번호 
	 */
	private int m_no;

	/**
	 *  게시물 제목
	 */
	public String title;

	/**
	 * 게시물 내용 
	 */
	public String body;
	
	/**
	 *  (미사용) 태그 
	 */
	public String tags;

	/**
	 * 공지, 일반게시판용, 커뮤니티 구분   
	 */
	public String category;

	/**
	 * 가림 여부
	 */
	public int blind;

	/**
	 * 저장일
	 */
	public Date saved;
	
	/**
	 * 수정일
	 */
	public Date updated;

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public int getM_no() {
		return m_no;
	}

	public void setM_no(int m_no) {
		this.m_no = m_no;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public int getBlind() {
		return blind;
	}

	public void setBlind(int blind) {
		this.blind = blind;
	}

}
