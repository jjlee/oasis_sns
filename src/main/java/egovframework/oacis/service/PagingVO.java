package egovframework.oacis.service;

/**
 * 페이지 네이션 VO
 * */
public class PagingVO {

    /**
     * 한 페이지에 보일 개수
     * */
    public int recordCountPerPage = 10;

    /**
     * 페이징 리스트의 사이즈
     * */
    public int pageSize = 1;

    /**
     * 현재 페이지
     */
    public int page = 1;

    public int startIndex;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getRecordCountPerPage() {
        return recordCountPerPage;
    }

    public void setRecordCountPerPage(int recordCountPerPage) {
        this.recordCountPerPage = recordCountPerPage;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getStartIndex() {
        return (page - 1) * recordCountPerPage;
    }

}
