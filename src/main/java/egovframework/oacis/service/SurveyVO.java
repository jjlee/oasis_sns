package egovframework.oacis.service;

import java.util.Date;

public class SurveyVO extends PagingVO {
    /**
     * 게시물 일련번호
     */
    public int no;

    /**
     * 작성자 회원번호
     */
    private int m_no;

    /**
     *  게시물 제목
     */
    public String title;

    /**
     *  숨김 여부
     */
    public boolean blind;

    /**
     * 저장일
     */
    public Date saved;

    /**
     * 수정일
     */
    public Date updated;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getM_no() {
        return m_no;
    }

    public void setM_no(int m_no) {
        this.m_no = m_no;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getSaved() {
        return saved;
    }

    public void setSaved(Date saved) {
        this.saved = saved;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public boolean isBlind() {
        return blind;
    }

    public void setBlind(boolean blind) {
        this.blind = blind;
    }
}
