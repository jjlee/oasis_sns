package egovframework.oacis.service;

import java.util.Date;

public class BusinessVO extends PagingVO {
    private int no;
    private String bName;
    private String bCountry;
    private String bCity;
    private String bRegion;
    private String bZip;
    private String bAddr;
    private String bLocationAlt;
    private String bLocationLat;
    private String bChargerName;
    private String bChargerContact;
    private String bChargerEmail;
    private String agent;
    private String agentCharger;
    private String agentContact;
    private String agentEmail;
    private String bBusinessDesc;
    private Date saved;
    private Date updated;
    private String updateYn;
    private String creater;

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public String getUpdateYn() {
        return updateYn;
    }

    public String getbBusinessDesc() {
        return bBusinessDesc;
    }

    public void setbBusinessDesc(String bBusinessDesc) {
        this.bBusinessDesc = bBusinessDesc;
    }

    public void setUpdateYn(String updateYn) {
        this.updateYn = updateYn;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getbName() {
        return bName;
    }

    public void setbName(String bName) {
        this.bName = bName;
    }

    public String getbCountry() {
        return bCountry;
    }

    public void setbCountry(String bCountry) {
        this.bCountry = bCountry;
    }

    public String getbCity() {
        return bCity;
    }

    public void setbCity(String bCity) {
        this.bCity = bCity;
    }

    public String getbRegion() {
        return bRegion;
    }

    public void setbRegion(String bRegion) {
        this.bRegion = bRegion;
    }

    public String getbZip() {
        return bZip;
    }

    public void setbZip(String bZip) {
        this.bZip = bZip;
    }

    public String getbAddr() {
        return bAddr;
    }

    public void setbAddr(String bAddr) {
        this.bAddr = bAddr;
    }

    public String getbLocationAlt() {
        return bLocationAlt;
    }

    public void setbLocationAlt(String bLocationAlt) {
        this.bLocationAlt = bLocationAlt;
    }

    public String getbLocationLat() {
        return bLocationLat;
    }

    public void setbLocationLat(String bLocationLat) {
        this.bLocationLat = bLocationLat;
    }

    public String getbChargerName() {
        return bChargerName;
    }

    public void setbChargerName(String bChargerName) {
        this.bChargerName = bChargerName;
    }

    public String getbChargerContact() {
        return bChargerContact;
    }

    public void setbChargerContact(String bChargerContact) {
        this.bChargerContact = bChargerContact;
    }

    public String getbChargerEmail() {
        return bChargerEmail;
    }

    public void setbChargerEmail(String bChargerEmail) {
        this.bChargerEmail = bChargerEmail;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getAgentCharger() {
        return agentCharger;
    }

    public void setAgentCharger(String agentCharger) {
        this.agentCharger = agentCharger;
    }

    public String getAgentContact() {
        return agentContact;
    }

    public void setAgentContact(String agentContact) {
        this.agentContact = agentContact;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public Date getSaved() {
        return saved;
    }

    public void setSaved(Date saved) {
        this.saved = saved;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
