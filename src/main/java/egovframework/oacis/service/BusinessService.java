package egovframework.oacis.service;

import java.util.HashMap;
import java.util.List;

public interface BusinessService {
    int registBusiness(BusinessVO businessVO);
    int updateBusiness(BusinessVO businessVO);
    int deleteBusiness(BusinessVO businessVO);
    List<HashMap> selectBusinessList(BusinessVO businessVO);
    HashMap selectBusiness(BusinessVO businessVO);
    int selectBusinessCount();

    int insertReply(HashMap param);
    List<HashMap> selectReply(int no);
    /// 검색
    List<HashMap> searchBusiness(String searchWord);
}
