<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<jsp:include page="header.jsp"></jsp:include>

<%
    /*
1. 리스크에 대한 이해
https://www.youtube.com/watch?v=O4GmLGGlk5U&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=2&t=3s
https://www.youtube.com/watch?v=-jo2Fh9Qty0&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=3&t=0s
https://www.youtube.com/watch?v=YBRuDz_8wCc&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=3
https://www.youtube.com/watch?v=7xY4omfKdpg&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=4
2. 신용등급에 대한 이해
https://www.youtube.com/watch?v=9OhDEsEZquQ&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=6&t=0s
https://www.youtube.com/watch?v=F9HNXcEYnmk&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=7
https://www.youtube.com/watch?v=a890HRMIRnM&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=6
https://www.youtube.com/watch?v=SHheqt2jMeo&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=8
3. 구조화 금융에 대한 이해
https://www.youtube.com/watch?v=wK2dKnEjuoM&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=11&t=0s
https://www.youtube.com/watch?v=Tr9LVvS2r4U&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=11
https://www.youtube.com/watch?v=mOmPPKOTxic&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=12
4. Project Financing
https://www.youtube.com/watch?v=uqb2dj6MwBo&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=14&t=0s
https://www.youtube.com/watch?v=p7lx4Clxrxg&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=14
https://www.youtube.com/watch?v=x3cVUnRAYeM&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=15
https://www.youtube.com/watch?v=ycEqao-FnmA&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=16
*/

%>

<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
<section>
    <div class='page_title_block'>
        <p>EDUCATION</p>
        <div>교육 및 기타자료</div>
    </div>

    <block>
        <p class='align_center'>
            1. 리스크에 대한 이해
        </p>
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/O4GmLGGlk5U' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/-jo2Fh9Qty0' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/YBRuDz_8wCc' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/7xY4omfKdpg' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
    </block>

    <block>
        <p class='align_center'>
            2. 신용등급에 대한 이해
        </p>
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/9OhDEsEZquQ' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/F9HNXcEYnmk' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/a890HRMIRnM' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/SHheqt2jMeo' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
    </block>

    <block>
        <p class='align_center'>
            3. 구조화 금융에 대한 이해
        </p>
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/wK2dKnEjuoM' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/Tr9LVvS2r4U' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/mOmPPKOTxic' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
    </block>

    <block>
        <p class='align_center'>
            4. Project Financing
        </p>
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/uqb2dj6MwBo' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/p7lx4Clxrxg' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/x3cVUnRAYeM' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
        <div style='position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;'>
            <iframe width='560' height='315' src='https://www.youtube.com/embed/ycEqao-FnmA' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen style='position: absolute; width:100%; height:100%;'></iframe>
        </div><br />
    </block>

</section>
<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->




<jsp:include page="footer.jsp"></jsp:include>