<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:include page="header.jsp"></jsp:include>
<script src="<c:url value="/js/highlight.js"/>"></script>
		<%
			String searchWord = request.getParameter("search_word");
			if (searchWord == null) {
				searchWord = "";
			}
		%>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<form name="search" method="get" action="/search.do">
			<div class="page_title_block">
				<p>SEARCH</p>
				<div style="margin:-2px 0 0 0;">
					검색
					<input class="search_txt" type="text" name="search_word" style="border:#333 1px solid;" value="<%=searchWord%>">
					<input class="search_btn" type="image" src="images/search.png" alt="Submit" width="30" height="30">
				</div>
			</div>
			</form>
			<ul class="none">
				<li>
					<b>검색 결과</b>
				</li>
				<%
					if (searchWord != "") {
				%>
				<li><b>개발사업</b></li>
				<c:set var="count" value="${fn:length(bList)}" />
				<c:if test="${count == 0}">
					<li class="">
						검색 결과가 없습니다.
					</li>
				</c:if>
				<c:forEach var="l" items="${bList}" varStatus="status">
					<li class="brief" onclick="showDetail(<c:out value="${l.no}" />)">
						[<c:out value="${l.date}" />] <c:out value="${l.name}" />
					</li>
				</c:forEach>
				<li><b>게시물</b></li>
				<c:set var="count" value="${fn:length(aList)}" />
				<c:if test="${count == 0}">
					<li class="">
						검색 결과가 없습니다.
					</li>
				</c:if>
				<c:forEach var="l" items="${aList}" varStatus="status">
					<li class="brief" onclick="showDetail(<c:out value="${l.no}" />)">
						[<c:out value="${l.categoryName}" />] [<c:out value="${l.date}" />] <c:out value="${l.title}" />
					</li>
				</c:forEach>
				<% } %>
			</ul>
			<div class="align_center">
				<input class="dlink" type="button" onclick="reset()" value="검색결과 초기화">
			</div>
		</section>
		<script>

			$("li.brief").highlight('<%=searchWord%>');

			function search() {

			}
			function reset() {
				location.href="./search.do";
			}
		</script>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>