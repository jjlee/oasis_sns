<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<jsp:include page="header.jsp"></jsp:include>

<%
    /*
[1차 사업발굴발표회]
베트남 하노이-하이퐁 간 스마트 냉장냉동 물류창고 개발사업
https://www.youtube.com/watch?v=x3XCYbK3ch4&list=PLDvMlxzTIf7lS_E902VDyNrxP0Qi5Ajbb&index=2&t=0s
파키스탄 복합 버스 터미널 프로젝트 개발사업
https://www.youtube.com/watch?v=pFJ22osSzp4&list=PLDvMlxzTIf7lS_E902VDyNrxP0Qi5Ajbb&index=3&t=0s
스리랑카 레지던스 아파트 개발 및 분양사업
https://www.youtube.com/watch?v=UAzVMmeEVtg&list=PLDvMlxzTIf7lS_E902VDyNrxP0Qi5Ajbb&index=3
스리랑카 가정용 태양광 사업
https://www.youtube.com/watch?v=CWL6HAO_i1I&list=PLDvMlxzTIf7lS_E902VDyNrxP0Qi5Ajbb&index=4
베트남 SSF (Small & Smart Factory) 아파트형 공장 리모델링 사업
https://www.youtube.com/watch?v=za3cDmxW5vE&list=PLDvMlxzTIf7lS_E902VDyNrxP0Qi5Ajbb&index=5
우즈베키스탄 점적테이프 생산공장 건설사업
https://www.youtube.com/watch?v=_FC2XvSIxc0&list=PLDvMlxzTIf7lS_E902VDyNrxP0Qi5Ajbb&index=6
베트남 박닌성 종합병원 건설사업
https://www.youtube.com/watch?v=E8dYCIC7iwA&list=PLDvMlxzTIf7lS_E902VDyNrxP0Qi5Ajbb&index=7
*/
%>

<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
<section>
    <div class="page_title_block">
        <p>BUSINESS</p>
        <div>사업발굴 발표회</div>
    </div>

    <submenu>
        <a href="#showcase02">2차 사업 발표회</a>
        <a href="#showcase01">1차 사업 발표회</a>
    </submenu>

    <block id="showcase02"></block>
    <block>
        <p class="align_center">
            포럼 및 2차 사업 발표회 행사 방송 동영상
        </p>
        <div style="position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;">
            <iframe width="560" height="315" src="https://youtu.be/7Zo5Wxw0ph0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" style="position: absolute; width:100%; height:100%;"></iframe>
        </div>
    </block>

    <br>
    <block id="showcase01"></block>
    <block>
        <p class="align_center">
            베트남 하노이-하이퐁 간 스마트 냉장냉동 물류창고 개발사업
        </p>
        <div style="position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/x3XCYbK3ch4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" style="position: absolute; width:100%; height:100%;"></iframe>
        </div>
    </block>

    <block>
        <p class="align_center">
            파키스탄 복합 버스 터미널 프로젝트 개발사업
        </p>
        <div style="position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/pFJ22osSzp4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" style="position: absolute; width:100%; height:100%;"></iframe>
        </div>
    </block>

    <block>
        <p class="align_center">
            스리랑카 레지던스 아파트 개발 및 분양사업
        </p>
        <div style="position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/UAzVMmeEVtg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" style="position: absolute; width:100%; height:100%;"></iframe>
        </div>
    </block>

    <block>
        <p class="align_center">
            스리랑카 가정용 태양광 사업
        </p>
        <div style="position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/CWL6HAO_i1I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" style="position: absolute; width:100%; height:100%;"></iframe>
        </div>
    </block>

    <block>
        <p class="align_center">
            베트남 SSF (Small &amp; Smart Factory) 아파트형 공장 리모델링 사업
        </p>
        <div style="position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/za3cDmxW5vE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" style="position: absolute; width:100%; height:100%;"></iframe>
        </div>
    </block>

    <block>
        <p class="align_center">
            우즈베키스탄 점적테이프 생산공장 건설사업
        </p>
        <div style="position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/_FC2XvSIxc0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" style="position: absolute; width:100%; height:100%;"></iframe>
        </div>
    </block>

    <block>
        <p class="align_center">
            베트남 박닌성 종합병원 건설사업
        </p>
        <div style="position: relative; height:0; padding-bottom: 56.25%; padding-left: 0px;">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/E8dYCIC7iwA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" style="position: absolute; width:100%; height:100%;"></iframe>
        </div>
    </block>


</section>
<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<%

//    1. 리스크에 대한 이해
//    https://www.youtube.com/watch?v=O4GmLGGlk5U&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=2&t=3s
//    https://www.youtube.com/watch?v=-jo2Fh9Qty0&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=3&t=0s
//    https://www.youtube.com/watch?v=YBRuDz_8wCc&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=3
//    https://www.youtube.com/watch?v=7xY4omfKdpg&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=4
//    2. 신용등급에 대한 이해
//    https://www.youtube.com/watch?v=9OhDEsEZquQ&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=6&t=0s
//    https://www.youtube.com/watch?v=F9HNXcEYnmk&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=7
//    https://www.youtube.com/watch?v=a890HRMIRnM&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=6
//    https://www.youtube.com/watch?v=SHheqt2jMeo&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=8
//    3. 구조화 금융에 대한 이해
//    https://www.youtube.com/watch?v=wK2dKnEjuoM&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=11&t=0s
//    https://www.youtube.com/watch?v=Tr9LVvS2r4U&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=11
//    https://www.youtube.com/watch?v=mOmPPKOTxic&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=12
//    4. Project Financing
//    https://www.youtube.com/watch?v=uqb2dj6MwBo&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=14&t=0s
//    https://www.youtube.com/watch?v=p7lx4Clxrxg&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=14
//    https://www.youtube.com/watch?v=x3cVUnRAYeM&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=15
//    https://www.youtube.com/watch?v=ycEqao-FnmA&list=PLDvMlxzTIf7lvDBooxDSBspnGRuAutiMj&index=16

%>
<jsp:include page="footer.jsp"></jsp:include>