<%@ page import="egovframework.oacis.service.MemberVO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:include page="header.jsp"></jsp:include>
<%
	//치환 변수 선언
	pageContext.setAttribute("crcn", "\r\n");
	pageContext.setAttribute("br", "<br>");
%>
<script type="text/javascript">
	function modifyBusiness(){
		document.modifyForm.action = "/viewRegistBusiness.do";
		document.modifyForm.submit();
	}
</script>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>BUSINESS</p>
				<div>인프라 사업</div>
			</div>
			<ul class='none'>
				<li>
					<b>사업명</b>  ${business["bName"]}
				</li>
				<li>
					<b>국가 / 도시</b>${business["bCountry"]} / ${business["bCity"]}
				</li>
				<li>
					<b>담당자 / 직위</b> ${business["bChargerName"]}
				</li>
				<li>
					<b>연락처</b> ${business["bChargerContact"]}
				</li>
				<li>
					<b>에이전트</b> ${business["agent"]}
				</li>
				<li>
					<b>에이전트 딤당자</b> ${business["agentCharger"]}
				</li>
				<li>
					<b>에이전트 연락처</b> ${business["agentContact"]}
				</li>
				<li>
					<b>이메일</b> ${business["agentEmail"]}
				</li>
				<li>
					<b>사업 개요</b>
				</li>
				<li class='line150'>
					${business["bBusinessDesc"]}
				</li>
			</ul>
			<div class='align_center'>
				<input class='dlink' type='button' value='목록으로'  onclick="location.href = '/business.do'" />
				<c:if test="${(sessionScope.memberVO.admin eq '9' && sessionScope.memberVO ne null) }" >
				<input class='dlink' type='button' value='정보수정'  onclick="modifyBusiness()" />
				</c:if>
			</div>
			<form method="post" name="modifyForm">
				<input type="hidden" name="updateYn" value="Y" />
				<input type="hidden" name="no" value = "${business["no"]}" />
			</form>
			<%
				MemberVO vo = (MemberVO) session.getAttribute("memberVO");
			%>
			<form class="form_normal" name="reply_register" method="post" action="">
				<ul class="none">
					<li class="article">
						<b>응답글</b>
					</li>
					<c:set var="count" value="${fn:length(list)}" />
					<c:if test="${count == 0}">
						<li class="article">
							응답글이 없습니다.
						</li>
					</c:if>
					<c:forEach var="l" items="${list}" varStatus="status">
					<li class="article">
						<c:out value="${l.NAME}" /> [<c:out value="${l.DATE}" />]
						<br><c:out value="${fn:replace(l.REPLY, crcn, br)}" />
					</li>
					</c:forEach>
					<% if (null != vo) { %>
					<!-- 로그인 상태일 때 -->
					<li>
						<b>응답글 작성</b>
					</li>
					<input type="hidden" name="member_no" id="member_no" value="<%=vo.getNo()%>">
					<input type="hidden" name="article_no" id="article_no" value="${business["no"]}">
					<textarea name="article_reply" id="article_reply" required=""></textarea>
					<% } %>
				</ul>
				<% if (null != vo) { %>
				<input type="button" onclick="sendReply();" value="응답글 저장">
				<% } %>
			</form>
		</section>
		<script>
			function sendReply() {
				$.ajax({
					url: 'bsnsReply.do',
					type: 'post',
					data: $("form[name=reply_register]").serialize(),
					success: function(data) {
						if (data == "1") {
							location.reload();
						}
						else {
							alert("응답글 작성중 에러가 발생했습니다.\n 잠시 후 다시 시도해 주세요.");
						}
					}
				})
			}
		</script>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>