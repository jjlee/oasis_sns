<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<jsp:include page="header.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
	<section>
		<div class="page_main_block" style="">
			<p>OASIS - Overseas Assistance of Strategic Investment for SME</p>
		</div>
	</section>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
	<section>
		<div class="notice_block">
			<p>공지사항</p>
			<ul class="none notice">
				<c:forEach var="l" items="${list}" varStatus="status">
				<li class='brief' onclick="noticeDetail(<c:out value="${l.no}" />)">
					[<c:out value="${l.date}" />] <c:out value="${l.title}" />
				</li>
				</c:forEach>
			</ul>
		</div>
	</section>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
	<section>
		<div class="align_center">
			<div class="page_list_block" onclick="showMenu('bsnsDevtPt', '')">
				<img src="images/img_pt.png" id="main_link1">
				<p>BUSINESS</p>
				<div>사업발굴 발표회</div>
			</div>
			<div class="page_list_block" onclick="showMenu('trnnAndOthrData', '')">
				<img src="images/img_edu.png" id="main_link2">
				<p>RESEARCH</p>
				<div>교육 및 기타자료</div>
			</div>
			<div class="page_list_block" id="page_matrix1" onclick="showMenu('business', '')">
				<img src="images/img_business.png">
				<p>BUSINESS</p>
				<div>해외 인프라 사업 정보</div>
			</div>
			<div class="page_list_block" id="page_matrix2" onclick="showMenu('financing', '')">
				<img src="images/img_fund.png">
				<p>FINANCING</p>
				<div>펀드레이징 기관 정보</div>
			</div>
			<div class="page_list_block" id="page_matrix3" onclick="showMenu('board', 'COMM_01')">
				<img src="images/img_community.png">
				<p>COMMUNITY1</p>
				<div>사업 참여자 포럼</div>
			</div>
			<div class="page_list_block" id="page_matrix4" onclick="showMenu('survey', '')">
				<img src="images/img_survey.png">
				<p>SURVEY</p>
				<div>해외 인프라 사업 설문조사</div>
			</div>
			<div class="page_list_block" id="page_matrix5" onclick="showMenu('board', 'NOTICE')">
				<img src="images/img_board.png">
				<p>BOARD</p>
				<div>공지, 문서, 멀티미디어</div>
			</div>
			<div class="page_list_block" id="page_matrix6" onclick="showMenu('board', 'COMM_02')">
				<img src="images/img_community.png">
				<p>COMMUNITY2</p>
				<div>전문가 포럼</div>
			</div>
		</div>
	</section>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
	<div class="float" id="float" style="display:none;">
		<section>
			<p id="float_text">&nbsp;</p>
			<div onclick="floatClose();">✖</div>
		</section>
	</div>
	<script>
		function noticeDetail(noticeNo) {
			var form = $("form#detail");
			form.attr("action", "boardDetail.do");
			form.append($('<input />', {type: 'hidden', name:'category', value:'NOTICE'}));
			form.append($('<input />', {type: 'hidden', name:'no', value:noticeNo}));
			form.submit();
		}
		function floatClose() {
			document.getElementById('float').style.display='none';
		}

	</script>			
<jsp:include page="footer.jsp"></jsp:include>