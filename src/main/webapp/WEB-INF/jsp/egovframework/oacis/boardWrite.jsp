<%@ page import="egovframework.oacis.service.MemberVO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:include page="header.jsp"></jsp:include>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
	<% MemberVO vo = (MemberVO) session.getAttribute("memberVO");

	if (vo == null) {
	%>
<script>
	alert("잘못된 요청입니다.");
	location.href="./"
</script>
	<%
	}
	%>
	<section>
		<div class='page_title_block'>
			<p>BOARD</p>
			<div><c:out value="${catInfo.cat_name}" /></div>
		</div>
		<form class='form_normal' name='article_form'>
			<input type="hidden" name="category" value="<c:out value="${catInfo.category}" />" />
			<input type="hidden" name="m_no" value="<%=vo.getNo()%>" />
			<p>제목</p>
			<input type='text' name='title' required />
			<p>내용</p>
			<textarea id="body" class='article_body' name='body'></textarea>
			<input type='button' value='저 장' onclick="showSave();" />
		</form>
		<div class='align_center'>
			<input class='dlink' type='button' value='게시물 삭제' />
			<input class='dlink' type='button' value='목록으로' />
		</div>
	</section>
	<script>
		function showSave() {
			if (confirm("게시글을 저장하시겠습니까?")) {
				var form = $("form.form_normal");
				form.attr("action", "actionBoardWrite.do");
				form.submit();
			}
		}
	</script>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>