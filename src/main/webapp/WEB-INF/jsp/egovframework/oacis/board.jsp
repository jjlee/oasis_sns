<%@ page import="egovframework.oacis.service.MemberVO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<jsp:include page="header.jsp"></jsp:include>

		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
			<div class='page_title_block'>
				<p>BOARD</p>
				<div><c:out value="${catInfo.cat_name}" /></div>
			</div>
			<ul class='none'>
				<c:forEach var="l" items="${list}" varStatus="status">
				<li class="brief" onclick="showDetail(<c:out value="${l.no}" />)">
					[<c:out value="${l.date}" />] <c:out value="${l.title}" />
				</li>
				</c:forEach>
			</ul>
			<div class="align_center">
				<div class="paging_div">
					<ui:pagination paginationInfo = "${paginationInfo}"
								   type="oasis"
								   jsFunction="showPage"/>
				</div>
			</div>
			<br>
			<div class="line150">
			</div>
			<% MemberVO vo = (MemberVO) session.getAttribute("memberVO"); %>
<%--			<c:out value="${catInfo.category}" />--%>
			<c:if test="${(sessionScope.memberVO.admin eq '9' && catInfo.category eq 'NOTICE') || (catInfo.category ne 'NOTICE' && sessionScope.memberVO ne null) }" >
			<div class="line150">
				게시물을 작성하려면 아래의 버튼을 클릭하세요. <input class="dlink" type="button" value="게시물 쓰기" onclick="showWrite();">
			</div>
			</c:if>
			<c:if test="${catInfo.category ne 'NOTICE'}" >
			<div class="align_center">
				<br/>
				<br>커뮤니티 그룹<br>
				<br/>
				<a href="javascript:showMenu('board', 'COMM_01')"><div class="page_list_block">
					<img src="images/img_community.png">
					<p>COMMUNITY</p>
					<div>참여자 커뮤니티</div>
				</div></a>
				<a href="javascript:showMenu('board', 'COMM_02')"><div class="page_list_block">
					<img src="images/img_community.png">
					<p>COMMUNITY</p>
					<div>전문가 커뮤니티</div>
				</div></a>
			</div>
			</c:if>
		</section>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
		<script>

			function showPage(page) {
				var form = $("form#detail");
				form.attr("action", "board.do");
				form.append($('<input />', {type: 'hidden', name:'category', value:'<c:out value="${catInfo.category}" />'}));
				form.append($('<input />', {type: 'hidden', name:'page', value: page}));
				form.submit();
			}

			function showDetail(article_no) {
				var form = $("form#detail");
				form.attr("action", "boardDetail.do");
				form.append($('<input />', {type: 'hidden', name:'category', value:'<c:out value="${catInfo.category}" />'}));
				form.append($('<input />', {type: 'hidden', name:'no', value:article_no}));
				form.submit();
			}

			function showWrite() {
				var form = $("form#detail");
				form.attr("action", "boardWrite.do");
				form.append($('<input />', {type: 'hidden', name:'category', value:'<c:out value="${catInfo.category}" />'}));
				form.submit();
			}

		</script>
<jsp:include page="footer.jsp"></jsp:include>