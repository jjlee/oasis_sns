<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
<jsp:include page="header.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>INTRODUCTION</p>
				<div><c:out value="${contents.comment}" /></div>
			</div>
			<p>
				<c:out value="${contents.contents}" escapeXml="false"/>
			</p>
		</section>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>