<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:include page="header.jsp"></jsp:include>
<script type="text/javascript">
	window.onload = function () {
		var serviceVal = document.getElementById("fService").value;
		if(serviceVal != 'undefiend' || serviceVal != '' || serviceVal != null){
			if(serviceVal.substring(0,1) == 1){
				document.getElementById('bank_service1').checked = true;
			}
			if(serviceVal.substring(1,2) == 1){
				document.getElementById('bank_service2').checked = true;
			}
			if(serviceVal.substring(2,3) == 1){
				document.getElementById('bank_service3').checked = true;
			}
			if(serviceVal.substring(3,4) == 1){
				document.getElementById('bank_service4').checked = true;
			}
			if(serviceVal.substring(4,5) == 1){
				document.getElementById('bank_service5').checked = true;
			}
		}
	}
	function registFinance(){
		var chkVal = "";
		var chkBox = document.getElementsByName("bank_service");
		for(var i = 0; i< chkBox.length; i++){
			if(chkBox[i].checked){
				chkVal += "1";
			}else{
				chkVal += "0";
			}
		}
		var elem = document.getElementById("no");
		if(typeof elem.value === 'undefined' && elem.value === null || elem.value === "") {
			document.getElementById("no").value = 0;
		}
		document.getElementById('fService').value = chkVal;
		document.bank_register.action = "/registFinance.do";
		document.bank_register.submit();
	}
</script>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>FINANCING</p>
				<div>투자 플랫폼 정보 등록</div>
			</div>
			<form class='form_normal' name='bank_register' method="post">
				<p>플랫폼명</p>
				<input type='text' name='fName' required value="<c:out value='${financeData.fName}'/>" />
				<p>플랫폼 유형 선택</p>
				<select name='fType'  value="<c:out value='${financeData.fType}'/>" >
					<option>정책금융 기관</option>
					<option>민간투자 플랫폼</option>
				</select>
				<p>우편번호</p>
				<input type='text' name='fZip' required value="<c:out value='${financeData.fZip}'/>" />
				<p>기관주소</p>
				<input type='text' name='fAddr' required value="<c:out value='${financeData.fAddr}'/>" />
				<p>투자방식</p>
				<input type='checkbox' class='bank_check' id='bank_service1' name='bank_service' value="1" required /><label for='bank_service1'>교육훈련 지원</label><br />
				<input type='checkbox' class='bank_check' id='bank_service2' name='bank_service' value="2" required /><label for='bank_service2'>정보 지원</label><br />
				<input type='checkbox' class='bank_check' id='bank_service3' name='bank_service' value="3" required /><label for='bank_service3'>기자재 반출입 지원</label><br />
				<input type='checkbox' class='bank_check' id='bank_service4' name='bank_service' value="4" required /><label for='bank_service4'>자금 지원</label><br />
				<input type='checkbox' class='bank_check' id='bank_service5' name='bank_service' value="5" required /><label for='bank_service5'>보증 지원</label>

				<p>담당자</p>
				<input type='text' name='fChargerName' required value="<c:out value='${financeData.fChargerName}'/>" />
				<p>연락처</p>
				<input type='text' name='fChargerContact' required value="<c:out value='${financeData.fChargerContact}'/>" />
				<p>이메일</p>
				<input type='text' name='fChargerEmail' required value="<c:out value='${financeData.fChargerEmail}'/>" />
				<p>플랫폼 소개</p>
				<textarea name='fChargerDesc' class="article_body"><c:out value='${financeData.fChargerDesc}' /></textarea>
				<input type='button' value='저 장' onclick="registFinance()" />
				<input type='hidden' id='fService' name="fService" value="<c:out value='${financeData.fService}'/>" />
				<input type="hidden" name="updateYn" value="<c:out value='${financeData.updateYn}' />" />
				<input type="hidden" name="no" id="no" value="<c:out value='${financeData.no}' />" />
			</form>
			<div class='align_center'>
				<input class='dlink' type='button' value='취소' />
			</div>
		</section>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>
