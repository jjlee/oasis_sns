<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>COMMUNITY</p>
				<div>커뮤니티</div>
			</div>
			<ul class='none'>
				<li class='article bold'>
					화성탐사선을 보내 퍼시비어런스가 수집한 화성 표본
				</li>
				<li class='article align_right'>
					홍길동님이 2020-07-30에 작성
				</li>
				<li class='article'>
					공공시설에 대한 종래의 운영방식만으로는 시민들의 라이프 스타일 변화에 대응하기에는 한계가 있다.
					이에 각 지자체에서는 도서관을 활용하여 시민들의 요구를 충족시키고 지역 활성화에 기여할 수 있는 방안에 대해 고민하고 있다.
					그 한 사례가 오부세정(町)이라는 인구 1만 명의 소도시에 있는 공립도서관이다. 오부세도서관은 시설 규모가 크지 않다는 점을 역이용하여 마을 전체를 도서관으로 만든다는 발상에서 동네도서관 프로젝트를 시작하였다. 
				</li>
			</ul>
			<div class='align_center'>
				<input class='dlink' type='button' value='게시물 수정' />
				<input class='dlink' type='button' value='목록으로' />
			</div>
			<form class='form_normal' name='business_register' >
			<ul class='none'>
				<li class='article'>
					<b>응답글</b>
				</li>
				<li class='article'>
					최영수 [7/21]<br />화성탐사선을 보내 퍼시비어런스가 수집한 화성 화성탐사선을 보내 퍼시비어런스가 수집한 화성 화성탐사선을 보내 퍼시비어런스가 수집한 화성
				</li>
				<li class='article'>
					최영수 [7/21]<br />화성탐사선을 보내 퍼시비어런스가 수집한 화성
				</li>
				<li class='article'>
					최영수 [7/21]<br />화성탐사선을 보내 퍼시비어런스가 수집한 화성
				</li>
				<li class='article'>
					최영수 [7/21]<br />화성탐사선을 보내 퍼시비어런스가 수집한 화성
				</li>
				<li class='article'>
					최영수 [7/21]<br />화성탐사선을 보내 퍼시비어런스가 수집한 화성
				</li>
				<li>
					<b>응답글 작성</b>
				</li>
				<textarea name='article_reply'></textarea>
			</ul>
			<input type='submit' value='응답글 저장' />
			</form>
		</section>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->


		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>COMMUNITY</p>
				<div>커뮤니티</div>
			</div>
			<form class='form_normal' name='article_form' >
				<p>제목</p>
				<input type='text' name='article_title' required />
				<p>내용</p>
				<textarea class='article_body' name='article_body'></textarea>
				<input type='submit' value='저 장' />
			</form>
			<div class='align_center'>
				<input class='dlink' type='button' value='게시물 삭제' />
				<input class='dlink' type='button' value='목록으로' />
			</div>
		</section>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>