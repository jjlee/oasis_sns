<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ page import="egovframework.oacis.service.MemberVO" %>
<jsp:include page="header.jsp"></jsp:include>
<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
<section>
    <div class='page_title_block'>
        <p>SURVEY</p>
        <div><c:out value="${info.title}" /></div>
    </div>
    <form class='form_normal' name='survey_respond' >
        <ul class='none'>
            <c:forEach var="l" items="${qList}" varStatus="status">
                <li><c:out value="${status.count}" />. <c:out value="${l.question}" /></li>
                <c:forEach var="a" items="${l.answers}" varStatus="status">
                    <c:choose>
                        <c:when test="${a.subs eq '_TEXT_'}">
                        <input type='text' name='ans_<c:out value="${a.no}" />' placeholder="응답을 입력하세요."/>
                        </c:when>
                        <c:otherwise>
                        <input type='radio' class='bank_check' id="ans_<c:out value="${a.no}" />" name='ans_<c:out value="${l.no}" />' />
                        <label for='ans_<c:out value="${a.no}" />'><c:out value="${a.subs}" /></label><br />
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </c:forEach>
            <%
                MemberVO vo = (MemberVO) session.getAttribute("memberVO");
                if(vo != null) {
            %>
            <input type='submit' value='저 장' />
            <% }%>
        </ul>
    </form>
    <div class='align_center'>
        <input class="dlink" type="button" value="목록으로" onclick="showList();">
    </div>
</section>
<script>

    function save() {
        
    }

    function showList() {
        var form = $("form#detail");
        form.attr("action", "survey.do");
        form.append($('<input />', {type: 'hidden', name: 'page', value: '<c:out value="${surveyVO.page}" />'}));
        form.submit();
    }
</script>
<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->

<jsp:include page="footer.jsp"></jsp:include>