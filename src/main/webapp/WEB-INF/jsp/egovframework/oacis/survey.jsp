<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ page import="egovframework.oacis.service.MemberVO" %>
<jsp:include page="header.jsp"></jsp:include>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>SURVEY</p>
				<div>설문조사 목록</div>
			</div>
			<% MemberVO vo = (MemberVO) session.getAttribute("memberVO"); %>
			<% if (vo == null) { %>
			<div class="align_center" style="padding:0 0 12px 0; color:crimson;">설문에 응답하려면 로그인이 필요합니다.</div>
			<% } %>
			<ul class='none'>
				<c:forEach var="l" items="${list}" varStatus="status">
					<li class="brief" onclick="showDetail(<c:out value="${l.no}" />)">
						[<c:out value="${l.date}" />] <c:out value="${l.title}" />
					</li>
				</c:forEach>
			</ul>
			<div class='align_center'>
				<div class="paging_div">
					<ui:pagination paginationInfo = "${paginationInfo}"
								   type="oasis"
								   jsFunction="showPage"/>
				</div>
			</div>
		</section>
		<script>
			function showDetail(no) {
				var form = $("form#detail");
				form.attr("action", "surveyDetail.do");
				form.append($('<input />', {type: 'hidden', name:'no', value:no}));
				form.submit();
			}
		</script>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>