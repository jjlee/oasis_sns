<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>
<script type="text/javascript">
	function login(){
		document.frm.action = "/actionLogin.do";
		document.frm.submit();

	}
</script>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<div id="fb-root"></div>
<script type="text/javascript" src="https://static.nid.naver.com/js/naveridlogin_js_sdk_2.0.0.js" charset="utf-8"></script>		
<script type="text/javascript" src="https://developers.kakao.com/sdk/js/kakao.js" charset="utf-8"></script>	
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ko_KR/sdk.js#xfbml=1&version=v9.0&appId=678845818823992&autoLogAppEvents=1" nonce="j6mBmsQU"></script>
		<section>
			<div class='page_title_block'>
				<p>MEMBERS</p>
				<div>회원 로그인</div>
			</div>
			<form class='form_normal' name='frm' method="post" >
				<p>이메일</p>
				<input type='text' name='email' required />
				<p>패스워드</p>
				<input type='password' name='passwd' required />
				<input type='button' value='로그인' onclick="login()"/>
				<input type="hidden" name="message" value="${message}" />
			</form>
			<div class='align_center'>
				<input class='dlink' type='button' value='회원 가입' onclick="location.href='join.do';"/>
				<input class='dlink' type='button' value='패스워드 재전송' />
			</div>
			<!-- 네이버 -->
			<div class='align_center'>
				<div id="naverIdLogin">
				<img alt="네이버아이디로로그인" height="50" width="231" src="/img/naver/naver_login_green.png"/>
				</div><br/>
				<a id="custom-login-btn" href="javascript:loginWithKakao()"><img alt="카카오아이디로로그인" src="/img/kakao/kakao_login_large_narrow.png"/></a><br/>
				<div class="fb-login-button" data-width="" data-size="large" data-button-type="login_with" data-layout="default" data-auto-logout-link="false" data-use-continue-as="false">
				 Facebook으로 로그인
				</div>
			</div>
		</section>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>
<script>
	var naverLogin = new naver.LoginWithNaverId(
			{
				clientId: "${clientid_naver}",
				callbackUrl: "${callback_naver}",
				isPopup: true, /* 팝업을 통한 연동처리 여부 */
				loginButton: {color: "green", type: 3, height: 60} /* 로그인 버튼의 타입을 지정 */
			}
	);
	
	/* 설정정보를 초기화하고 연동을 준비 */
	naverLogin.init();
	/* (4) Callback의 처리. 정상적으로 Callback 처리가 완료될 경우 main page로 redirect(또는 Popup close) */
	window.addEventListener('load', function () {
		naverLogin.getLoginStatus(function (status) {
			if (status) {
				/* (5) 필수적으로 받아야하는 프로필 정보가 있다면 callback처리 시점에 체크 */
				var email = naverLogin.user.getEmail();
				if( email == undefined || email == null) {
					alert("이메일은 필수정보입니다. 정보제공을 동의해주세요.");
					/* (5-1) 사용자 정보 재동의를 위하여 다시 네아로 동의페이지로 이동함 */
					naverLogin.reprompt();
					return;
				}

				window.location.replace("http://" + window.location.hostname + ( (location.port==""||location.port==undefined)?"":":" + location.port) + "/sample/main.html");
			} else {
				console.log("callback 처리에 실패하였습니다.");
			}
		});
	});
	//-----------------
	
	//----------------
  	function loginWithKakao() {
    	Kakao.Auth.authorize({
      	redirectUri: 'https://developers.kakao.com/tool/demo/oauth'
    	})
  	}
  	// 아래는 데모를 위한 UI 코드입니다.
  	displayToken()
  	function displayToken() {
    	const token = getCookie('authorize-access-token')
    	if(token) {
      		Kakao.Auth.setAccessToken(token)
      		Kakao.Auth.getStatusInfo(({ status }) => {
        		if(status === 'connected') {
          			document.getElementById('token-result').innerText = 'login success. token: ' + Kakao.Auth.getAccessToken()
        		} else {
          			Kakao.Auth.setAccessToken(null)
        		}
      		})
    	}
  	}
  	function getCookie(name) {
    	const value = "; " + document.cookie;
    	const parts = value.split("; " + name + "=");
    	if (parts.length === 2) return parts.pop().split(";").shift();
 	}	
	//----------------
	function fnInit() {
		var message = document.frm.message.value;
		if(message == "fail"){
			alert("로그인에 실패하였습니다.");
		}else if(message == "success"){
			alert(message);
			location.href="/main.do";
		}
	}
	fnInit();
</script>