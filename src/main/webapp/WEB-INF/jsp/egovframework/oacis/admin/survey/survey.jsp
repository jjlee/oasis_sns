<%--
  Created by IntelliJ IDEA.
  User: Jinuk
  Date: 2020-08-30
  Time: 오전 1:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<jsp:include page="../isLogin.jsp"></jsp:include>
<jsp:include page="../header.jsp"></jsp:include>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<div class="contents_block">

    <jsp:include page="lnb.jsp"></jsp:include>

    <div class="right_block">

        <div class="content_title">
            설문 관리
        </div>
        <div class="content_subtitle">
            회원 대상의 설문을 관리합니다.
        </div>
        <div class="content_body">
            (1)설문의 생성, 수정, 삭제<br>
            (2)설문별 결과 데이터의 관리
        </div>
        <div class="content_body">

            <c:forEach var="l" items="${list}" varStatus="status">
                <div class="survey_lists" onclick="showDetail(<c:out value="${l.no}" />)">
                    <div>[<c:out value="${l.date}" />]</div>
                    <a><div><c:out value="${l.title}" /></div></a>
                    <div class="q_handler">
                        <a href="javascript:void(0);" onclick="showDetail(<c:out value="${l.no}" />)">수정</a>
                        <a href="javascript:void(0);" onclick="del(<c:out value="${l.no}" />)" title="주의! 즉시 삭제됩니다.">삭제</a>
                        <c:choose>
                            <c:when test="${l.blind eq false}">
                                <a href="javascript:void(0);" onclick="show(<c:out value="${l.no}" />, true)">공개중 - 가리기</a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:void(0);" onclick="show(<c:out value="${l.no}" />, false)">감춤중 - 공개하기</a>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </c:forEach>

        </div>
        <div class="align_center">
            <div class="paging_div">
                <ui:pagination paginationInfo = "${paginationInfo}"
                               type="oasis"
                               jsFunction="showPage"/>
            </div>
        </div>
        <script>
            function show(no, isShow) {
                $.ajax({
                    url: 'showSurvey.do',
                    type: 'post',
                    data: {"no": no, "blind": isShow},
                    success: function(data) {
                        if (data == "1") {
                            location.reload();
                        }
                        else {
                            alert("보이기 중 에러가 발생했습니다.\n 잠시 후 다시 시도해 주세요.");
                        }
                    }
                })
            }

            function del(no) {
                if (confirm("삭제하시겠습니까?")) {
                    $.ajax({
                        url: 'deleteSurvey.do',
                        type: 'post',
                        data: {"no": no},
                        success: function(data) {
                            if (data == "1") {
                                location.reload();
                            }
                            else {
                                alert("삭제 중 에러가 발생했습니다.\n 잠시 후 다시 시도해 주세요.");
                            }
                        }
                    })
                }
            }

            function showPage(page) {
                var form = $("form#form");
                form.attr("action", "survey.do");
                form.append($('<input />', {type: 'hidden', name:'page', value: page}));
                form.submit();
            }
        </script>
    </div>
</div>

<jsp:include page="../footer.jsp"></jsp:include>