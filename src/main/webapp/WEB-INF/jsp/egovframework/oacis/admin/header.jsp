<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ page import="egovframework.oacis.service.MemberVO" %>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <meta property="og:type" content="website">
    <meta property="og:title" content="OASIS">
    <meta property="og:description" content="오아시스, OASIS, Overseas Assistance of Construction Investment for SME">
    <meta property="og:image" content="https://oasis.krihs.re.kr/images/OASIS_LOGO1.png">
    <meta property="og:url" content="https://oasis.krihs.re.kr/">

    <title>OASIS - 중소·중견기업 소규모 인프라 해외사업 진출 지원 커뮤니티</title>
    <link rel='stylesheet' type='text/css' href="<c:url value="/css/admin.css"/>"/>
    <link rel='stylesheet' type='text/css' href="<c:url value="/css/admin_style.css"/>" />
    <link rel='stylesheet' type='text/css' href="<c:url value="/css/ckeditor.css"/>" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script>
        $(function(){
            var responseMessage = "<c:out value="${message}" />";
            if(responseMessage != ""){
                alert(responseMessage)
            }
        });
    </script>
</head>
<body>
    <div class="basic_block">
        오아시스 앱 데이터베이스 관리자
    </div>
    <div class="basic_block">
        <%
            MemberVO vo = (MemberVO) session.getAttribute("memberVO");
        %>
        <% if (null != vo && vo.getAdmin().equals("9")) { %>
<%--        <a class="top_menu" href="#">사업발표회 관리</a>--%>
        <a class="top_menu" href="/admin/business.do">인프라 사업 관리</a>
        <a class="top_menu" href="/admin/financing.do">기관 정보 관리</a>
<%--        <a class="top_menu" href="#">교육, 기타자료 관리</a>--%>
<%--        <a class="top_menu" href="/admin/community.do">커뮤니티 관리</a>--%>
        <a class="top_menu" href="/admin/survey.do">설문 관리</a>
        <a class="top_menu" href="/admin/board.do">게시판 관리</a>
        <a class="top_menu" href="/admin/member.do">회원 관리</a>
        <a class="top_menu" href="/admin/contents.do">콘텐츠 관리</a>
        <a class="top_menu" href="javascript:logout();" style="float:right; background-color:crimson;">로그아웃</a>
        <%
           }
           else {
        %>
        <a class="top_menu" href="/admin/login.do">관리자 로그인</a>
        <% } %>
    </div>
    <script>
        function logout() {
            if (confirm("로그아웃 하시겠습니까?")) {
                location.href="/admin/logout.do";
            }
        }
    </script>