<%--
  Created by IntelliJ IDEA.
  User: Jinuk
  Date: 2020-08-30
  Time: 오전 12:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <form action="" method="post" id="form">
    <script src="https://cdn.ckeditor.com/ckeditor5/21.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '.ckeditor' ), {
                ckfinder: {
                    uploadUrl: '${pageContext.request.contextPath}/fileUpload.do'
                },
                alignment: {
                    options: [ 'left', 'center', 'right' ]
                }
            } )
            .then( editor => {
                console.log( 'Editor was initialized', editor );
                myEditor = editor;
            } )
            .catch( error => {
                console.error( error );
            } );
    </script>
    </body>
</html>