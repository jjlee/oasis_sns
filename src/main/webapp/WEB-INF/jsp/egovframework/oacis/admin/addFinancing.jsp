<%--
  Created by IntelliJ IDEA.
  User: Jinuk
  Date: 2020-08-30
  Time: 오전 1:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:include page="isLogin.jsp"></jsp:include>
<jsp:include page="header.jsp"></jsp:include>
<script type="text/javascript">
    window.onload = function () {
        var serviceVal = document.getElementById("fService").value;
        if(serviceVal != 'undefiend' || serviceVal != '' || serviceVal != null){
            if(serviceVal.substring(0,1) == 1){
                document.getElementById('bank_service1').checked = true;
            }
            if(serviceVal.substring(1,2) == 1){
                document.getElementById('bank_service2').checked = true;
            }
            if(serviceVal.substring(2,3) == 1){
                document.getElementById('bank_service3').checked = true;
            }
            if(serviceVal.substring(3,4) == 1){
                document.getElementById('bank_service4').checked = true;
            }
            if(serviceVal.substring(4,5) == 1){
                document.getElementById('bank_service5').checked = true;
            }
        }
    }
    function registFinance(){
        var chkVal = "";
        var chkBox = document.getElementsByName("bank_service");
        for(var i = 0; i< chkBox.length; i++){
            if(chkBox[i].checked){
                chkVal += "1";
            }else{
                chkVal += "0";
            }
        }
        if(document.getElementById("no").value == ""){
            document.getElementById("no").value = 0;
        }
        document.getElementById('fService').value = chkVal;
        document.new_input_form.action = "/admin/registFinancing.do";
        document.new_input_form.submit();
    }
</script>
<div class="contents_block">
    <div class="left_block">
        <%--        <a href="?">left menu1</a>--%>
        <%--        <a href="?">left menu2</a>--%>
        <%--        <a href="?">left menu3</a>--%>
        <%--        <a href="?">left menu4</a>--%>
    </div>

    <div class="right_block">

        <div class="content_title">
            금융 정보 관리
        </div>
        <div class="content_subtitle">
            금융 정보를 관리합니다.
        </div>
        <div class="content_body">
            게시 정보의 등록
        </div>

        <br>
        <div class="content_subtitle">
            펀드 정보 등록
        </div>

        <div class="content_body">
            <form name="new_input_form" method="post">
                <input type="text" id="fName" name="fName"  placeholder="기관명" value="">
                <select name='fType'  value="" >
                    <option>정책금융 기관</option>
                    <option>민간투자 플랫폼</option>
                </select>
                <input type="text" id="fZip" name="fZip"  placeholder="우편번호" value="">
                <input type="text" id="fAddr" name="fAddr"  placeholder="기관 주소" value="">

                <p>지원 방식</p>
                <input type='checkbox' class='bank_check' id='bank_service1' name='bank_service' value="1" required /><label for='bank_service1'>교육훈련 지원</label><br />
                <input type='checkbox' class='bank_check' id='bank_service2' name='bank_service' value="2" required /><label for='bank_service2'>정보 지원</label><br />
                <input type='checkbox' class='bank_check' id='bank_service3' name='bank_service' value="3" required /><label for='bank_service3'>기자재 반출입 지원</label><br />
                <input type='checkbox' class='bank_check' id='bank_service4' name='bank_service' value="4" required /><label for='bank_service4'>자금 지원</label><br />
                <input type='checkbox' class='bank_check' id='bank_service5' name='bank_service' value="5" required /><label for='bank_service5'>보증 지원</label>

                <input type="text" name="fChargerName"  placeholder="담당자 이름" value="">
                <input type="text" name="fChargerContact"  placeholder="담당자 연락처" value="" >
                <input type="text" name="fChargerEmail"  placeholder="담당자 이메일" value="" >
                <textarea id="fChargerDesc" name="fChargerDesc" placeholder="상세정보" class="ckeditor"></textarea>
                <input type='hidden' id='fService' name="fService" value="" />
                <input type="hidden" name="updateYn" value="" />
                <input type="hidden" id="no" name="no" value="" />
                <input type="submit" value="저장"  onclick="registFinance()" >
            </form>
            <br>
        </div>
    </div>
</div>

<jsp:include page="footer.jsp"></jsp:include>