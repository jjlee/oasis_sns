<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<jsp:include page="isLogin.jsp"></jsp:include>
<jsp:include page="header.jsp"></jsp:include>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<div class="contents_block">

    <jsp:include page="business/lnb_business.jsp"></jsp:include>

    <div class="right_block">
        <div class="content_title">
            인프라 사업 관리
        </div>
        <div class="content_subtitle">
            인프라 사업 정보를 관리합니다.
        </div>
        <div class="content_body">
            게시 정보의 수정, 삭제
        </div>
        <br>
        <div class="content_subtitle">
            사업 정보 수정
        </div>
        <div class="content_body">
            <c:forEach var="l" items="${businessList}" varStatus="status">
                <div class="biz_lists">
                    <div>[<c:out value="${l.bName}" />]</div>
                    <div>(<c:out value="${l.bCountry}" />/<c:out value="${l.bCity}" />)</div>
                    <div><c:out value="${l.bRegion}" /></div>
                    <div>: <c:out value="${l.bChargerName}" /></div>
                    <div> &nbsp;
                        <a href="javascript:updateBusiness(<c:out value="${l.no}" />);">수정</a>
                        <a href="javascript:deleteBusiness(<c:out value="${l.no}" />);" title="주의! 즉시 삭제됩니다.">삭제</a>
                    </div>
                    <div> <c:out value="${l.bBusinessDesc}" />);"</div>
                </div>
            </c:forEach>

            <ui:pagination paginationInfo = "${paginationInfo}"
                           type="oasis"
                           jsFunction="showPage"/>
        </div>

        <script>
            function updateBusiness(bNO) {
                location.href="/admin/modifyBusiness.do?no=" + bNO;
            }
            function deleteBusiness(bNO) {
                if(confirm('사업 정보를 정말로 삭제하시겠습니까?')) {
                    location.href="/admin/deleteBusiness.do?no=" + bNO;
                }
            }
        </script>
    </div>
</div>

<jsp:include page="footer.jsp"></jsp:include>