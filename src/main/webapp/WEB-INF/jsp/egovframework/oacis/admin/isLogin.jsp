<%--
  Created by IntelliJ IDEA.
  User: Jinuk
  Date: 2020-08-30
  Time: 오전 12:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="egovframework.oacis.service.MemberVO" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
    if (null == memberVO || !memberVO.getAdmin().equals("9")) {
%>
    <script>
        alert("권한이 없습니다.");
        location.href="login.do";
    </script>
<%
    }
%>