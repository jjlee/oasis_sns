<%--
  Created by IntelliJ IDEA.
  User: Jinuk
  Date: 2020-08-30
  Time: 오전 1:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<jsp:include page="../isLogin.jsp"></jsp:include>
<jsp:include page="../header.jsp"></jsp:include>

<div class="contents_block">

    <jsp:include page="lnb_community.jsp"></jsp:include>

    <div class="right_block">

        <div class="content_title">
            커뮤니티 관리
        </div>
        <div class="content_subtitle">
            커뮤니티를 관리합니다.
        </div>
        <div class="content_body">
            (1)커뮤니티 생성, 수정, 삭제<br>
        </div>

        <br>
        <div class="content_subtitle">
            커뮤니티 정보 입력
            <a href="?pid=community">[NEW]</a>
        </div>
        <div class="content_body">
            <c:forEach var="l" items="${list}" varStatus="status">
            <div class="board_lists">
                <div>[<c:out value="${l.category}" />]</div>
                <div><c:out value="${l.name}" /></div>
                <div> &nbsp;
                    <a href="javascript:void(0);" onclick="detailComm(<c:out value="${l.no}" />);">수정</a>
                    <a href="javascript:void(0);" onclick="deleteBoard(<c:out value="${l.no}" />);" title="주의! 즉시 삭제됩니다.">삭제</a>
                </div>
                <div><c:out value="${l.exp}" /></div>
            </div>
            </c:forEach>
        </div>
        <script>
            function detailComm(bNo) {
                // debugger;
                var form = $("form[name=action]")
                form.attr("action", "/admin/modifyCommunity.do");
                form.append($('<input />', {type: 'hidden', name:'no', value:bNo}));
                form.submit();
            }
            function deleteBoard(bNO) {
                if(confirm('정말로 삭제하시겠습니까?')) {
                    var form = $("form[name=action]")
                    form.attr("action", "/admin/modifyCommunity.do");
                    form.append($('<input />', {type: 'hidden', name:'no', value:bNo}));
                    form.submit();
                }
            }
        </script>
    </div>
</div>

<jsp:include page="../footer.jsp"></jsp:include>