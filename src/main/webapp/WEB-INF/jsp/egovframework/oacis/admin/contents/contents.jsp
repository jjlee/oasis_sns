<%--
  Created by IntelliJ IDEA.
  User: Jinuk
  Date: 2020-08-30
  Time: 오후 5:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<jsp:include page="../isLogin.jsp"></jsp:include>
<jsp:include page="../header.jsp"></jsp:include>

<div class="contents_block">
    <div class="left_block">
        <a href="/admin/contents.do">콘텐츠 목록</a>
<%--        <a href="/admin/contents.do">콘텐츠 등록</a>--%>
    </div>

    <div class="right_block">
        <div class="content_title">
            콘텐츠 관리
        </div>
        <div class="content_subtitle">
            사이트에 게시되는 주요 콘텐츠를 관리합니다.
        </div>
        <div class="content_body">
            (1)사이트 소개 정보의 수정<br>
            (2)사이트 이용 안내 정보의 수정<br>
            (3)사이트 회원 약관 정보의 수정<br>
            (4)사이트 개인정보 보호 방침 정보의 수정
        </div>
        <br/>
        <div class="content_body">
            <c:forEach var="l" items="${list}" varStatus="status">
                <div class="contents_lists">
                    <a href="javascript:void(0);" onclick="showDetail('<c:out value="${l.title}" />')"><c:out value="${l.comment}" /></a>
                </div>
            </c:forEach>
        </div>
        <script>
            function showDetail(title) {
                var form = $("form#form")
                form.attr("action", "/admin/modifyContents.do");
                form.append($('<input />', {type: 'hidden', name:'title', value:title}));
                form.submit();
            }
        </script>
    </div>
</div>

<jsp:include page="../footer.jsp"></jsp:include>