<%--
  Created by IntelliJ IDEA.
  User: Jinuk
  Date: 2020-08-30
  Time: 오후 11:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<jsp:include page="../isLogin.jsp"></jsp:include>
<jsp:include page="../header.jsp"></jsp:include>

<div class="contents_block">
    <div class="left_block">
        <a href="/admin/contents.do">콘텐츠 목록</a>
        <%--        <a href="/admin/contents.do">콘텐츠 등록</a>--%>
    </div>

    <div class="right_block">
        <div class="content_title">
            콘텐츠 관리
        </div>
        <div class="content_subtitle">
            사이트에 게시되는 주요 콘텐츠를 관리합니다.
        </div>
        <div class="content_body">
            (1)사이트 소개 정보의 수정<br>
            (2)사이트 이용 안내 정보의 수정<br>
            (3)사이트 회원 약관 정보의 수정<br>
            (4)사이트 개인정보 보호 방침 정보의 수정
        </div>
        <br/>
        <div class="content_body">
            <form method="post" action="/admin/actionTexts.do" target="_self">
                <input type="hidden" name="text_no" value="<c:out value="${c.no}" />">
                <input type="hidden" name="text_title" value="<c:out value="${c.title}" />">
                <input type="text" name="text_comment" value="<c:out value="${c.comment}" />" placeholder="콘텐츠 제목">
                <textarea name="text_body" class="ckeditor"><c:out value="${c.contents}" /></textarea>
                <br>
                <input type="submit" value="저장">
                <script>

                </script>
            </form>

            <br><a class="click_carefully" href="javascript:delContents(1);">콘텐츠 삭제</a>
            <script>
                function delContents(cNO) {
                    if(confirm('이 콘텐츠를 삭제하시겠습니까?')) {
                        var delURL='?xid=contents&xmode=delete&cno='+cNO;
                        document.getElementById('data_exec_frame').src=delURL;
                    }
                }
            </script>
        </div>

    </div>
</div>

<jsp:include page="../footer.jsp"></jsp:include>