<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="left_block">
    <c:forEach var="l" items="${cList}" varStatus="status">
        <a href='javascript:void(0)' onclick='showArticles("<c:out value="${l.category}" />");'><c:out value="${l.categoryName}" /></a>
    </c:forEach>
</div>
<script>
    function showArticles(category) {
        var form = $("form#form");
        form.attr("action", "board.do");
        form.append($('<input />', {type: 'hidden', name:'category', value:category}));
        form.submit();
    }
</script>
