<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Jinuk
  Date: 2020-08-30
  Time: 오전 1:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<jsp:include page="../isLogin.jsp"></jsp:include>
<jsp:include page="../header.jsp"></jsp:include>

<jsp:include page="lnb_member.jsp"></jsp:include>

<div class="right_block">

    <div class="content_title">
        회원 관리
    </div>
    <div class="content_subtitle">
        회원 정보를 관리합니다.
    </div>
    <div class="content_body">
        (1)회원 정보 및 활동의 관리
    </div>

    <br>
    <div class="content_body">
        <c:forEach var="l" items="${list}" varStatus="status">
        <div class="biz_lists" onclick="showDetail(<c:out value="${l.no}" />)">
            <div>
                <c:out value="${l.name}" />
                <c:if test="${l.admin eq 9}">
                    <a>관리자</a>
                </c:if>
            </div>
            <div>
                <c:out value="${l.email}" />
            </div>
        </div>
        </c:forEach>
    </div>
    <div class="align_center">
        <div class="paging_div">
            <ui:pagination paginationInfo = "${paginationInfo}"
                           type="oasis"
                           jsFunction="showPage"/>
        </div>
    </div>
</div>

<jsp:include page="../footer.jsp"></jsp:include>