<%--
  Created by IntelliJ IDEA.
  User: Jinuk
  Date: 2020-08-30
  Time: 오전 1:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:include page="isLogin.jsp"></jsp:include>
<jsp:include page="header.jsp"></jsp:include>
<script>
    function registBusiness(){
        if(document.getElementById("no").value == ""){
        document.getElementById("no").value = 0;
        }
        document.new_input_form.action = "/admin/registBusiness.do";
        document.new_input_form.submit();
    }

</script>
<div class="contents_block">
    <div class="left_block">
        <%--        <a href="?">left menu1</a>--%>
        <%--        <a href="?">left menu2</a>--%>
        <%--        <a href="?">left menu3</a>--%>
        <%--        <a href="?">left menu4</a>--%>
    </div>

    <div class="right_block">

        <div class="content_title">
            인프라 개발 사업 관리
        </div>
        <div class="content_subtitle">
            콘인프라 개발 사업 정보를 등록합니다.
        </div>
        <div class="content_body">
            게시 정보의 등록
        </div>

        <br>
        <div class="content_subtitle">
            사업 정보 등록
        </div>
        <div class="content_body">
            <form name="new_input_form" method="post">
                <input type="text" name="bName" id="bName" placeholder="사업명" value="" />
                <input type="text" name="bCountry" id="bCountry" placeholder="사업 진행 국가" value="" />
                <input type="text" name="bCity" id="bCity" placeholder="사업 진행 도시" value="" />
                <input type="text" name="bChargerName" id="bChargerName" placeholder="담당자" value="" />
                <input type="text" name="bChargerContact" id="b_chargerContact" placeholder="연락처" value="" />
                <input type="text" name="bChargerEmail" id="bChargerEmail" placeholder="이메일" value="" />
                <input type="text" name="agent" id="agent" placeholder="에이전트" value="" />
                <input type="text" name="agentCharger" id="agentCharger" placeholder="에이전트 담당자" value="" />
                <input type="text" name="agentContact" id="agentContact" placeholder="에이전트 연락처" value="" />
                <input type="text" name="agentEmail" id="agentEmail" placeholder="에이전트 이메일" value="" />
                <input type="text" name="fund" id="fund" placeholder="파이낸싱(펀드명)" value="" />
                <textarea name="bBusinessDesc" id="description" placeholder="사업 개요" class="ckeditor"></textarea>
                <input type="submit" value="저장" onclick="registBusiness()">
                <input type="hidden" name="updateYn" value="" />
                <input type="hidden" id="no" name="no" value="" />
            </form>
        </div>
    </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>