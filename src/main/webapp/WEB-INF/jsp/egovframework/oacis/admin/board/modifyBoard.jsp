<%--
  Created by IntelliJ IDEA.
  User: Jinuk
  Date: 2020-08-30
  Time: 오전 1:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<jsp:include page="../isLogin.jsp"></jsp:include>
<jsp:include page="../header.jsp"></jsp:include>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="contents_block">
    <jsp:include page="lnb_board.jsp"></jsp:include>
    <div class="right_block">
        <div class="content_title">
            게시판 관리
        </div>
        <div class="content_subtitle">
            사이트(앱)에서 제공하는 게시판을 관리합니다.
        </div>
        <div class="content_body">
            (1)게시판의 생성, 수정, 삭제<br>
            (2)운영 규칙을 위반한 게시물의 블라인드 처리<br>
            (3)공지사항 및 관리자용 게시판 게시물의 등록 및 관리
        </div>

        <div class="content_body">
            <form class='form_normal' name='article_form' action="actionModifyBoard.do" method="post">
                <input type="hidden" name="category" value="<c:out value="${article.category}" />" />
                <input type="hidden" name="no" value="<c:out value="${article.no}" />" />
                <p>제목</p>
                <input type='text' name='title' required value="<c:out value="${article.title}" />"/>
                <p>내용</p>
                <textarea id="body" class='ckeditor' name='body'><c:out value="${article.body}" escapeXml="false"/></textarea>
                <br/>
                <input type="submit" value="저장">
            </form>
        </div>
        <script>
            function saveBoard() {

            }
        </script>
    </div>
</div>
<jsp:include page="../footer.jsp"></jsp:include>