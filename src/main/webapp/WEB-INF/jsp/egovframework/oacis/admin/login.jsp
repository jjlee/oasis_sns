<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:include page="header.jsp"></jsp:include>

<div class="contents_block">
    <div class="left_block">
        <a href="/admin/login.do">관리자 로그인</a>
    </div>
    <div class="right_block">
        <div class="content_title">
            관리자 로그인
        </div>
        <br>
        <div class="content_subtitle">
            로그인 정보 입력
        </div>
        <form name="new_input_form" method="post" action="/admin/actionLogin.do">
            <input type="text" name="email" id="admin_id" placeholder="관리자 아이디(이메일)" required="">
            <input type="password" name="passwd" id="admin_pass" placeholder="관리자 패스워드" required="">
            <input type="submit" value="로그인">
        </form>
    </div>
</div>

<jsp:include page="footer.jsp"></jsp:include>