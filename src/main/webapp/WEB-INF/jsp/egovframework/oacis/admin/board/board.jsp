<%--
  Created by IntelliJ IDEA.
  User: Jinuk
  Date: 2020-08-30
  Time: 오전 1:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<jsp:include page="../isLogin.jsp"></jsp:include>
<jsp:include page="../header.jsp"></jsp:include>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>

<div class="contents_block">
    <jsp:include page="lnb_board.jsp"></jsp:include>
    <div class="right_block">
        <div class="content_title">
            게시판 관리
        </div>
        <div class="content_subtitle">
            사이트(앱)에서 제공하는 게시판을 관리합니다.
        </div>
        <div class="content_body">
            (1)게시판의 생성, 수정, 삭제<br>
            (2)운영 규칙을 위반한 게시물의 블라인드 처리<br>
            (3)공지사항 및 관리자용 게시판 게시물의 등록 및 관리
        </div>

        <div class="content_body">
        <c:forEach var="l" items="${list}" varStatus="status">
            <div class="board_lists">
                <div><c:out value="${l.cat_name}" /></div>
                <div>[<c:out value="${l.date}" />] <c:out value="${l.title}" /></div>
                <div> &nbsp;
                    <a href="#" onclick="mod(<c:out value="${l.no}" />)">수정</a>
                    <a href="#" onclick="del(<c:out value="${l.no}" />)" title="주의! 즉시 삭제됩니다.">삭제</a>
                    <c:choose>
                        <c:when test="${l.blind eq 0}">
                            <a href="#" onclick="hide(<c:out value="${l.no}" />)">공개중 - 가리기</a>
                        </c:when>
                        <c:otherwise>
                            <a href="#" onclick="show(<c:out value="${l.no}" />)">감춤중 - 공개하기</a>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </c:forEach>
        </div>
        <div class="align_center">
            <div class="paging_div">
            <ui:pagination paginationInfo = "${paginationInfo}"
                           type="oasis"
                           jsFunction="showPage"/>
            </div>
        </div>
        <script>
            function mod(no) {
                var form = $("form#form");
                form.attr("action", "modifyBoard.do");
                form.append($('<input />', {type: 'hidden', name:'category', value:'<c:out value="${articlesVO.category}" />'}));
                form.append($('<input />', {type: 'hidden', name:'no', value: no}));
                form.submit();
            }

            function show(no) {
                $.ajax({
                    url: 'showArticle.do',
                    type: 'post',
                    data: {"no": no},
                    success: function(data) {
                        if (data == "1") {
                            location.reload();
                        }
                        else {
                            alert("보이기 중 에러가 발생했습니다.\n 잠시 후 다시 시도해 주세요.");
                        }
                    }
                })
            }

            function hide(no) {
                $.ajax({
                    url: 'hideArticle.do',
                    type: 'post',
                    data: {"no": no},
                    success: function(data) {
                        if (data == "1") {
                            location.reload();
                        }
                        else {
                            alert("가리기 중 에러가 발생했습니다.\n 잠시 후 다시 시도해 주세요.");
                        }
                    }
                })
            }

            function del(no) {
                $.ajax({
                    url: 'deleteArticle.do',
                    type: 'post',
                    data: {"no": no},
                    success: function(data) {
                        if (data == "1") {
                            location.reload();
                        }
                        else {
                            alert("삭제 중 에러가 발생했습니다.\n 잠시 후 다시 시도해 주세요.");
                        }
                    }
                })
            }

            function showPage(page) {
                var form = $("form#form");
                form.attr("action", "board.do");
                form.append($('<input />', {type: 'hidden', name:'category', value:'<c:out value="${articlesVO.category}" />'}));
                form.append($('<input />', {type: 'hidden', name:'page', value: page}));
                form.submit();
            }
        </script>
    </div>
</div>
<jsp:include page="../footer.jsp"></jsp:include>