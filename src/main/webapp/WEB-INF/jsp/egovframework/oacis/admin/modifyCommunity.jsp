<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Jinuk
  Date: 2020-08-30
  Time: 오후 5:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<jsp:include page="isLogin.jsp"></jsp:include>
<jsp:include page="header.jsp"></jsp:include>

<div class="contents_block">

    <jsp:include page="community/lnb_community.jsp"></jsp:include>

    <div class="right_block">

        <div class="content_title">
            커뮤니티 관리
        </div>
        <div class="content_subtitle">
            커뮤니티를 관리합니다.
        </div>
        <div class="content_body">
            (1)커뮤니티 생성, 수정, 삭제<br>
        </div>

        <br>
        <div class="content_subtitle">
            커뮤니티 정보 입력
        </div>
        <div class="content_body">
            <form name="new_input_form" method="post" action="/admin/actionCommunity.do">
                <input type="hidden" name="board_no" id="board_no" value="<c:out value="${c.no}" />">
                <input type="text" name="board_id" id="board_id" placeholder="커뮤니티 아이디(입력 아이디 앞에 'COMM_' 자동 생성)" required="" value="<c:out value="${c.category}" />">
                <input type="text" name="board_title" id="board_title" placeholder="커뮤니티 제목" required="" value="<c:out value="${c.name}" />">
                <input type="text" name="description" id="description" placeholder="설명" value="<c:out value="${c.exp}" />">
                <input type="submit" value="저장">
            </form>
            <br>
        </div>
    </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>