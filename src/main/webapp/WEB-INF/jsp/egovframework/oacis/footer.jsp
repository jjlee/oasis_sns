<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="egovframework.oacis.service.MemberVO" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
		<div class="bottom">
			<section>
				<div class="align_center">
					OASIS :
					<a href="http://www.krihs.re.kr/" target="_blank">국토연구원(krihs.re.kr)</a> 제공
				</div>
			</section>
		</div>
		<div class="foot">
			<section>
				&nbsp;
			</section>
		</div>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<div class="head">
			<section>
				<%
					MemberVO vo = (MemberVO) session.getAttribute("memberVO");
				%>
				<div class="align_center _site_logo">
					<a href=".">OASIS</a>
					<% if (null != vo) { %>
					<div class="_user">
						<a href="#"><%=vo.getName()%>님</a>
					</div>
					<% } %>
					<div class="_context">
						<a href="javascript:show_context();"><b>&nbsp;</b></a>
					</div>
					<div id="_context_block" class="_context_block" style="display:none;">
						<b><div id="triangle-up"></div></b>
						<a href="javascript:showMenu('search', '');" class='_context_menu'>통합검색</a>
						<a href="javascript:showMenu('oasis', '');" class='_context_menu'>OASIS 소개</a>
						<a href="javascript:showMenu('business', '');" class='_context_menu'>인프라 사업</a>
						<a href="javascript:showMenu('bsnsDevtPt', '');" class='_context_menu'>사업 발굴 발표회</a>
						<a href="javascript:showMenu('financing', '');" class='_context_menu'>관련 기관</a>
						<a href="javascript:showMenu('trnnAndOthrData', '');" class='_context_menu'>교육 및 기타자료</a>
						<a href="javascript:showMenu('survey', '');" class='_context_menu'>설문조사</a>
						<a href="javascript:showMenu('board', 'COMM_01');" class='_context_menu'>커뮤니티</a>
						<a href="javascript:showMenu('board', 'NOTICE');" class='_context_menu'>게시판</a>
						<% if (null != vo) { %>
						<!-- 로그인 상태일 때 -->
						<a href="javascript:showMenu('member', '');" class='_context_menu'>회원정보</a>
						<% } else { %>
						<!-- 로그아웃 상태일 때 -->
						<a href="javascript:showMenu('login', '');" class='_context_menu'>회원로그인</a>
						<% } %>
						<a href="javascript:showMenu('useGuide', '');" class='_context_menu'>이용안내</a>
					</div>
					
					<script>
						function show_menus() {
							var MenuBlock=document.getElementById('_menu_block');
							if(MenuBlock.style.display=='') {
								MenuBlock.style.display='none';
							}
							else {
								MenuBlock.style.display='';
							}
						}
						
						function show_context() {
							var MenuBlock=document.getElementById('_context_block');
							if(MenuBlock.style.display=='') {
								MenuBlock.style.display='none';
							}
							else {
								MenuBlock.style.display='';
							}
						}
					</script>
				</div>
			</section>
		</div>
		<form action="" method="post" id="detail">
		</form>
		<script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
		<script>
			ClassicEditor
					.create( document.querySelector( '.article_body' ), {
						ckfinder: {
							uploadUrl: '${pageContext.request.contextPath}/fileUpload.do'
						},
						alignment: {
							options: [ 'left', 'center', 'right' ]
						}
					} )
					.then( editor => {
						console.log( 'Editor was initialized', editor );
						myEditor = editor;
					} )
					.catch( error => {
						console.error( error );
					} );



			function showMenu(menu, boardCat) {
				var form = $("form#detail");
				form.attr("action", menu + ".do");
				if (boardCat != '') {
					form.append($('<input />', {type: 'hidden', name:'category', value:boardCat}));
				}
				form.submit();
			}
		</script>
	</body>
</html>