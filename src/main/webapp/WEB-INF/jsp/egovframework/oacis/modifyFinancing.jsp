<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:include page="header.jsp"></jsp:include>
<script type="text/javascript">
	function modifyFinance(){
		if(document.getElementById("no").value == ""){
			document.getElementById("no").value = 0;
		}
		document.modifyForm.action = "/viewRegistFinance.do";
		document.modifyForm.submit();
	}
</script>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>FINANCING</p>
				<div>투자 플랫폼 정보</div>
			</div>
			<ul class='none'>
				<li>
					<b>플랫폼명</b> ${finance["fName"]}
				</li>
				<li>
					<b>투자방식</b> ${finance["fServiceDetail"]}
				</li>
				<li>
					<b>담당자 / 직위</b> ${finance["fChargerName"]}
				</li>
				<li>
					<b>연락처</b>${finance["fChargerContact"]}
				</li>
				<li>
					<b>이메일</b> ${finance["fChargerEmail"]}
				</li>
				<li>
					<b>사업 개요</b>
				</li>
				<li class='line150'>
					${finance["fChargerDesc"]}
				</li>
			</ul>
			<div class='align_center'>
				<input class='dlink' type='button' value='목록으로' onclick="location.href = '/financing.do'" />
				<c:if test="${(sessionScope.memberVO.admin eq '9' && sessionScope.memberVO ne null) }" >
					<input class='dlink' type='button' value='정보수정' onclick="modifyFinance()" />
				</c:if>
			</div>
			<form method="post" name="modifyForm">
				<input type="hidden" name="updateYn" value="Y" />
				<input type="hidden" id="no" name="no" value = "${finance["no"]}" />
			</form>

		</section>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>