<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
	<section>
		<div class='page_title_block'>
			<p>MEMBERS</p>
			<div>마이페이지</div>
		</div>
		<ul class='none'>
			<li>
				<b>북마크</b>
			</li>
			<li class='brief'>
				[07/29] 북마크한 게시물 제목(글자수 줄임)
			</li>
			<li class='brief'>
				[07/29] 북마크한 게시물 제목
			</li>
			<li class='brief'>
				[07/29] 북마크한 게시물 제목
			</li>
		</ul>
		<ul class='none'>
			<li>
				<b>수행 사업</b>
			</li>
			<li class='brief'>
				[진행] 베트남 학교 건설
			</li>
			<li class='brief'>
				[완료] 베트남 도서관 건설
			</li>
			<li class='brief'>
				[완료] 베트남 태양광 발전소 건설
			</li>
		</ul>
		<ul class='none'>
			<li>
				<b>작성한 게시물</b>
			</li>
			<li class='brief'>
				[07/29] 내가 작성한 게시물 제목(글자수 줄임)
			</li>
			<li class='brief'>
				[07/29] 내가 작성한 게시물 제목
			</li>
			<li class='brief'>
				[07/29] 내가 작성한 게시물 제목
			</li>
		</ul>
		<div class='align_center'>
			<input class='dlink' type='button' value='회원정보 조회' />
		</div>
		<br /><br /><br /><br /><br />
	</section>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>