<%@ page import="egovframework.oacis.service.MemberVO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:include page="header.jsp"></jsp:include>
<%
	//치환 변수 선언
	pageContext.setAttribute("crcn", "\r\n");
	pageContext.setAttribute("br", "<br>");
%>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
	<section>
		<div class='page_title_block'>
			<p>BOARD</p>
			<div><c:out value="${catInfo.cat_name}" /></div>
		</div>
		<ul class='none'>
			<li class='article'>
				<c:out value="${article.title}" />
			</li>
			<li class='article align_right'>
				<c:out value="${article.name}" />님이 <c:out value="${article.date}" /> 에 작성
			</li>
			<li class='article'>
				<c:set var="body" value="${fn:replace(article.body, crcn, br)}" />
				<c:out value="${body}" escapeXml="false" />
			</li>
		</ul>
		<div class='align_center'>
			<%
				MemberVO vo = (MemberVO) session.getAttribute("memberVO");
				if(null != vo && vo.getAdmin() != null && vo.getAdmin().equals("9")) {
			%>
			<input class='dlink' type='button' value='게시물 수정' onclick="showModify()"/>
			<%  } %>
			<input class='dlink' type='button' value='목록으로' onclick="showList()"/>
		</div>
		<form class="form_normal" name="reply_register" method="post" action="">
			<ul class="none">
				<li class="article">
					<b>응답글</b>
				</li>
				<c:set var="count" value="${fn:length(list)}" />
				<c:if test="${count == 0}">
					<li class="article">
						응답글이 없습니다.
					</li>
				</c:if>
				<c:forEach var="l" items="${list}" varStatus="status">
					<li class="article">
						<c:out value="${l.NAME}" /> [<c:out value="${l.DATE}" />]
						<br><c:out value="${fn:replace(l.REPLY, crcn, br)}" escapeXml="false"/>
					</li>
				</c:forEach>
				<% if (null != vo) { %>
				<!-- 로그인 상태일 때 -->
				<li>
					<b>응답글 작성</b>
				</li>
				<input type="hidden" name="member_no" id="member_no" value="<%=vo.getNo()%>">
				<input type="hidden" name="article_no" id="article_no" value="<c:out value="${article.no}" />">
				<textarea name="article_reply" id="article_reply" required=""></textarea>
				<% } %>

			</ul>
			<% if (null != vo) { %>
			<input type="button" onclick="sendReply();" value="응답글 저장">
			<% } %>
		</form>
	</section>
	<script>
		function showList() {
			var form = $("form#detail");
			form.attr("action", "board.do");
			form.append($('<input />', {type: 'hidden', name: 'category', value: '<c:out value="${article.category}" />'}));
			form.submit();
		}

		function showModify() {
			var form = $("form#detail");
			form.attr("action", "modifyBoard.do");
			form.append($('<input />', {type: 'hidden', name: 'no', value: '<c:out value="${article.no}" />'}));
			form.append($('<input />', {type: 'hidden', name: 'category', value: '<c:out value="${article.category}" />'}));
			form.submit();
		}

		function sendReply() {
			$.ajax({
				url: 'actionArticleReply.do',
				type: 'post',
				data: $("form[name=reply_register]").serialize(),
				success: function(data) {
					if (data == "1") {
						location.reload();
					}
					else {
						alert("응답글 작성중 에러가 발생했습니다.\n 잠시 후 다시 시도해 주세요.");
					}
				}
			})
		}

	</script>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>