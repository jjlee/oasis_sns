<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:include page="header.jsp"></jsp:include>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
	<section>
		<div class='page_title_block'>
			<p>MEMBERS</p>
			<div>개인정보 보호</div>
		</div>
		<p>
			<c:out value="${contents}"></c:out>
		</p>
		<div class='align_center'>
			<input class='dlink' type='button' value='회원 약관'  onclick="location.href='joinTerm.do'"/>
			<input class='dlink' type='button' value='회원 가입'  onclick="location.href='join.do'"/>
		</div>
	</section>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>