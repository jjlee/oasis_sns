<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>MEMBERS</p>
				<div>패스워드 수정</div>
			</div>
			<form class='form_normal' name='member_repass' >
				<p>현재 패스워드</p>
				<input type='password' name='member_pass' required />
				<p>새로운 패스워드</p>
				<input type='password' name='member_pass1' required />
				<p>새로운 패스워드 재입력</p>
				<input type='password' name='member_pass2' required />
				<input type='submit' value='패스워드 수정' />
			</form>
			<div class='align_center'>
				<input class='dlink' type='button' value='회원정보 조회' />
			</div>
		</section>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->	
<jsp:include page="footer.jsp"></jsp:include>