<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<jsp:include page="header.jsp"></jsp:include>
<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
<section>
	<div class='page_title_block'>
		<p>BUSINESS</p>
		<div>인프라 사업 목록</div>
	</div>
	<ul class='none'>

		<c:forEach var="l" items="${businessList}" varStatus="status">
			<li class='brief' onclick="businessDetail(<c:out value="${l.no}" />)">
				[<c:out value="${l.updated}" />] <c:out value="${l.bName}" />
			</li>
		</c:forEach>
	</ul>
	<div class="paging_div">
		<ui:pagination paginationInfo = "${paginationInfo}"
					   type="oasis"
					   jsFunction="showPage"/>
	</div>
	<c:if test="${(sessionScope.memberVO.admin eq '9' && sessionScope.memberVO ne null) }" >
<%--	<div class='align_center'>--%>
<%--		<input class='dlink' type='button' name="regist" value='정보 등록하기' onclick="location.href='/viewRegistBusiness.do'" />--%>
<%--	</div>--%>
	</c:if>
</section>
<form name="business" method="get">
	<input name="no" id="no" type="hidden" />
</form>
<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>
<script type="text/javascript">
	function businessDetail(no) {
		document.business.action = "/viewUpdateBusiness.do";
		document.getElementById('no').value = no;
		document.business.submit();
	}
</script>