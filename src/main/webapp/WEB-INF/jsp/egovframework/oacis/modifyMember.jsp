<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>MEMBERS</p>
				<div>회원 정보 수정</div>
			</div>
			<form class='form_normal' name='member_join' >
				<p>이름</p>
				<input type='text' name='member_name' required />
				<p>이메일</p>
				<input type='text' name='member_email' required />
				<p>연락처(휴대폰)</p>
				<input type='text' name='member_mobile' required />
				<p>패스워드</p>
				<input type='password' name='member_pass' required />
				<p>패스워드 재입력</p>
				<input type='password' name='member_pass2' required />
				<p>간단 소개글</p>
				<textarea name='member_intro'></textarea>
				<input type='checkbox' id='member_check' name='member_check' required />
				<label for='member_check'>아래의 회원 약관 및 개인정보 보호 내용을 확인</label>
				<input type='submit' value='저 장' />
			</form>
			<div class='align_center'>
				<input class='dlink' type='button' value='회원 약관' onclick="location.href='joinTerm.do'"/>
				<input class='dlink' type='button' value='개인정보 보호' onclick="location.href='privacyTerm.do'" />
				<input class='dlink' type='button' value='회원정보 조회' onclick="location.href='member.do'" />
				<input class='dlink' type='button' value='회원 탈퇴' onclick="location.href=''"/>
			</div>
		</section>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>