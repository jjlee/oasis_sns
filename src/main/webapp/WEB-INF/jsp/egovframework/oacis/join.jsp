<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>
<script type="text/javascript">
	function fn_create_user(){
		document.member_join.action = "/registMember.do";
		document.member_join.submit();

	}
</script>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>MEMBERS</p>
				<div>회원 가입</div>
			</div>
			<form class='form_normal' name='member_join' method="post">
				<p>이름</p>
				<input type='text' name='name' required />
				<p>이메일</p>
				<input type='text' name='email' required />
				<p>연락처(휴대폰)</p>
				<input type='text' name='phone' required />
				<p>패스워드</p>
				<input type='password' name='passwd' required />
				<p>패스워드 재입력</p>
				<input type='password' name='member_pass2' required />
				<p>간단 소개글</p>
				<textarea name='hello'></textarea>
				<input type='checkbox' id='member_check' name='member_check' required />
				<label for='member_check'>아래의 회원 약관 및 개인정보 보호 내용을 확인</label>
				<input type="hidden" name="message" value="${message}" />
				<input type='button' value='저 장' onclick="fn_create_user()"/>
			</form>
			<div class='align_center'>
				<input class='dlink' type='button' value='회원 약관' onclick="location.href='joinTerm.do'"/>
				<input class='dlink' type='button' value='개인정보 보호' onclick="location.href='privacyTerm.do'" />
				<input class='dlink' type='button' value='회원 로그인'  onclick="location.href='login.do'" />
			</div>
		</section>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->

<jsp:include page="footer.jsp"></jsp:include>
<script>
	function fnInit() {
		var message = document.frm.message.value;
		if (message == "success") {
			alert(message);
			location.href="/login.do";
		}else if(message == "fail"){
			alert(message);
		}
	}
	fnInit();
</script>