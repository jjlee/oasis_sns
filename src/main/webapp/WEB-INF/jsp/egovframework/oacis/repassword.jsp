<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
	<section>
		<div class='page_title_block'>
			<p>MEMBERS</p>
			<div>패스워드 재발급</div>
		</div>
		<p>
			패스워드를 분실하신 경우 새로운 패스워드를 생성해서 등록된 이메일 주소로 전송해드립니다.
		</p>
		<form class='form_normal' name='member_repass' >
			<p>이메일</p>
			<input type='text' name='member_email' required />
			<input type='submit' value='재발급' />
		</form>
		<div class='align_center'>
			<input class='dlink' type='button' value='회원 로그인' />
		</div>
	</section>
	<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>