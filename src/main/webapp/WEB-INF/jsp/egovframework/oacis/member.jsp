<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="header.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>MEMBERS</p>
				<div>회원정보 조회</div>
			</div>
			<ul class='none'>
				<li>
					<b>회원명</b> <c:out value='${memberInfo.name}'/>
				</li>
				<li>
					<b>이메일</b> <c:out value='${memberInfo.email}'/>
				</li>
				<li>
					<b>연락처</b> <c:out value='${memberInfo.phone}'/>
				</li>
				<li>
					<b>소개글</b>
				</li>
				<li>
					<c:out value='${memberInfo.hello}'/>
				</li>
			</ul>
			<div class='align_center'>
				<input class='dlink' type='button' value='로그아웃' onclick="location.href='logout.do'"/>
				<input class='dlink' type='button' value='마이페이지' onclick="location.href='myPage.do'"/>
				<input class='dlink' type='button' value='회원정보 수정' onclick="location.href='modifyMember.do'" />
				<input class='dlink' type='button' value='패스워드 수정' onclick="location.href='modifyPwd.do'" />
			</div>
		</section>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>