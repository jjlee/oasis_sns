<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<jsp:include page="header.jsp"></jsp:include>
<script>
	function registBusiness(){
		var elem = document.getElementById("no");
		if(typeof elem.value === 'undefined' && elem.value === null || elem.value === "") {
			document.getElementById("no").value = 0;
		}
		document.business_register.action = "/registBusiness.do";
		document.business_register.submit();

	}

	function deleteBusiness(){
		if(document.getElementById("no").value == "" || document.getElementById("no").value == "undefined"){
			alert("삭제할수없습니다.");
			return;
		}
		document.business_register.action = "/deleteBusiness.do";
		document.business_register.submit();

	}
</script>

		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>BUSINESS</p>
				<div>인프라 개발 사업 등록</div>
			</div>
			<form class='form_normal' name='business_register' >
				<p>사업명</p>
				<input type='text' name='bName' required value="<c:out value='${businessData.bName}'/>" />
				<p>국가</p>
				<input type='text' name='bCountry' required value="<c:out value='${businessData.bCountry}'/>"  />
				<p>도시</p>
				<input type='text' name='bCity' required value="<c:out value='${businessData.bCity}'/>"  />
				<p>담당자</p>
				<input type='text' name='bChargerName' required value="<c:out value='${businessData.bChargerName}'/>" />
				<p>연락처</p>
				<input type='text' name='bChargerContact' required value="<c:out value='${businessData.bChargerContact}'/>"  />
				<p>이메일</p>
				<input type='text' name='bChargerContact' required value="<c:out value='${businessData.bChargerEmail}'/>"  />
				<p>에이전트</p>
				<input type='text' name='agent' required value="<c:out value='${businessData.agent}'/>"  />
				<p>에이전트 담당자</p>
				<input type='text' name='agentCharger' required value="<c:out value='${businessData.agentCharger}' />"  />
				<p>에이전트 연락처</p>
				<input type='text' name='agentContact' required value="<c:out value='${businessData.agentContact}'/>"  />
				<p>에이전트 이메일</p>
				<input type='text' name='agentEmail' required value="<c:out value='${businessData.agentEmail}'/>"  />
				<p>사업 개요</p>
				<textarea name='bBusinessDesc' class='article_body'>
					 <c:out value='${businessData.bBusinessDesc}' escapeXml="false"/>
				</textarea>
				<input type='submit' value='저 장'  onclick="registBusiness()" />
				<input type="hidden" name="updateYn" value="<c:out value='${businessData.updateYn}' />" />
				<input type="hidden" id="no" name="no" value="<c:out value='${businessData.no}' />" />
				<input type="hidden" id="creater" name="creater" value="" />
			</form>
			<div class='align_center'>
				<input class='dlink' type='button' value='취소' />
				<input class='dlink' type='button' value='삭제' onclick="deleteBusiness()" />
			</div>
		</section>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>
