<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<jsp:include page="header.jsp"></jsp:include>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(시작) ----------------------->
		<section>
			<div class='page_title_block'>
				<p>FINANCING</p>
				<div>정책금융기관 목록</div>
			</div>
			<ul class='none'>
<%--				[<c:out value="${l.updated}" />]--%>
				<c:forEach var="l" items="${financeList}" varStatus="status">
					<li class='brief' onclick="financeDetail(<c:out value="${l.no}" />)">
						 <c:out value="${l.fName}" />
					</li>
				</c:forEach>
			</ul>
			<div class="paging_div">
				<ui:pagination paginationInfo = "${paginationInfo}"
							   type="oasis"
							   jsFunction="showPage"/>
			</div>
			<c:if test="${(sessionScope.memberVO.admin eq '9' && sessionScope.memberVO ne null) }" >
<%--			<div class='align_center'>--%>
<%--				<input class='dlink' type='button' name="regist" value='등록' onclick="location.href='/viewRegistFinance.do'" />--%>
<%--			</div>--%>
			</c:if>
		</section>
		<form name="financing" method="get">
			<input name="no" id="no" type="hidden" />
		</form>
		<!-------------------- 모든 페이지 콘텐츠의 수록 부분(종료) ----------------------->
<jsp:include page="footer.jsp"></jsp:include>
<script type="text/javascript">
	function financeDetail(no) {
		document.financing.action = "/viewUpdateFinance.do";
		document.getElementById('no').value = no;
		document.financing.submit();
	}
</script>



