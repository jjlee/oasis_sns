<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c"    uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ui"   uri="http://egovframework.gov/ctl/ui"%>
<!doctype html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		
		<meta property="og:type" content="website">
		<meta property="og:title" content="OASIS">
		<meta property="og:description" content="오아시스, OASIS, Overseas Assistance of Construction Investment for SME">
		<meta property="og:image" content="https://oasis.krihs.re.kr/images/OASIS_LOGO1.png">
		<meta property="og:url" content="https://oasis.krihs.re.kr/">

		<META http-equiv="Expires" content="-1">
		<META http-equiv="Pragma" content="no-cache">
		<META http-equiv="Cache-Control" content="No-Cache">

		<title>OASIS - 중소·중견기업 소규모 인프라 해외사업 진출 지원 커뮤니티</title>
		<link rel='stylesheet' type='text/css' href="<c:url value="/css/style.css"/>" />
		<link rel='stylesheet' type='text/css' href="<c:url value="/css/customize.css"/>" />
		<link rel='stylesheet' type='text/css' href="<c:url value="/css/media_queries.css"/>" />
		<link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR&amp;display=swap" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/font-kopub/1.0/kopubdotum.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		
	</head>
	<script>
		$(function(){
			var responseMessage = "<c:out value="${message}" />";
			if(responseMessage != ""){
				alert(responseMessage)
			}
		})
	</script>
	<body>
		<div class='head_space'>&nbsp;</div>		
		